//
//  SplashViewController.swift
//  Eventure
//
//  Created by Denislava on 28//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit
import FacebookCore

class SplashViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if AccessToken.current != nil {
            showMainScreen()
        } else {
            showLogin()
        }
    }
    
    func showMainScreen() {
        let main = getInstance(of: MainViewController.self)
        navigationController?.pushViewController(main, animated: true)
    }
    
    private func showLogin() {
        let login = getInstance(of: LoginViewController.self)
        present(login, animated: true, completion: nil)
    }
}
