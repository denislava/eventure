//
//  GetEventsResponse.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

struct GetEventsResponse: DeserializableModel {
    
    let events: [EventResponse]
    
    static func fromJSON(_ jsonData: [String : AnyObject]?) -> DeserializableModel? {
        guard let data = jsonData
            else { return nil }
        let events = EventResponse.buildArray(data["events"] as? [[String : AnyObject]])
        return GetEventsResponse(events: events)
    }
}
