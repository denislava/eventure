//
//  PagingManager.swift
//  PayTalkAgent
//
//  Created by IO_MAK01 on 3/30/17.
//  Copyright © 2017 Upnetix. All rights reserved.
//

import Foundation
/**
 Annotates that the implementing class has a PagingManager instance and
 can potentially handle pagination/infinite scroll
 */
protocol HasPager {
    var pagingManager: PagingManager {get}
}

typealias PagingJson = (key: String, object: [String: Any])

class PagingManager {
    private (set) var page: Int = 0
    private (set) var size: Int = 0
    private (set) var currentCount = 0
    private (set) var lastPageReached: Bool = false
    private var locked: Bool = false
    
    private init() {}
    
    init<T: Equatable>(withSize: Int, bindableArray: BindableArray<T>) {
        size = withSize
        bindableArray.bind { res in
            self.feedResult(results: res.count)
        }
    }
    
    func reset() {
        page = 0
        currentCount = 0
        lastPageReached = false
        locked = false
    }
    
    func getNext() throws ->  (page: Int, size: Int) {
        if lastPageReached {
            throw PagingError.lastPageReached(page: page, total: currentCount)
        }
        if !locked {
            page += 1
            locked = true
        }
        return (page: page, size: size)
    }
    
    func getNextJson(pageKey: String, sizeKey: String, objectKey: String) throws -> PagingJson {
        let tuple: (page: Int, size: Int)
        do {
            tuple = try getNext()
        } catch (let error) {
            throw error
        }
        return (key: objectKey, object: [
            pageKey: tuple.page,
            sizeKey: tuple.size
            ])
    }
    
    // TODO: Find a way to pass the array and privately check if last page reached
    private func feedResult(results: Int) {
        lastPageReached = results % size != 0 || results == currentCount
        print("RESULTS CAME")
        currentCount = results
        locked = false
    }
}

enum PagingError: Error {
    case lastPageReached(page: Int, total: Int)
}
