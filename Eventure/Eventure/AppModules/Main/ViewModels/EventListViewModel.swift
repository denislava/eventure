//
//  EventListViewModel.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

class EventListViewModel: BaseViewModel {
    typealias ApiContractorType = EventContract
    var apiContractor: ApiContractorType = EventContract()
    required init(apiContractor: ApiContractorType) {
        self.apiContractor = apiContractor
    }
    
    var allEvents: BindableArray<EventResponse> = BindableArray([])
    var myEvents: BindableArray<EventResponse> = BindableArray([])

    func getEvents() {
        apiContractor.getEvents { [weak self] (events, _) in
            guard let strongSelf = self,
                let events = events else { return }
            strongSelf.allEvents.value = events.events
            strongSelf.myEvents.value = events.events.filter({ (event) -> Bool in
                event.isAnswered == .yes
            })
        }
    }
    
    func answerEvent(isAnswered: Int, eventId: Int) {
        apiContractor.answerEvent(eventId: eventId, isAnswered: isAnswered) { (_, _) in
            //DO NOTHING
        }
    }
}
