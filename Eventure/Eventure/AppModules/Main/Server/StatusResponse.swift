//
//  StatusResponse.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

struct StatusResponse: DeserializableModel {
    let isSuccess: Bool
    static func fromJSON(_ jsonData: [String : AnyObject]?) -> DeserializableModel? {
        guard let data = jsonData,
            let success = data as? Bool else { return nil }
        return StatusResponse(isSuccess: success)
    }
}
