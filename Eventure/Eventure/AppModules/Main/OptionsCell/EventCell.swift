//
//  EventCell.swift
//  Eventure
//
//  Created by Denislava on 58//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit

protocol EventCellDelegate: class {
    func didSelectIsAnswered(isAnswered: Int, eventId: Int)
}

struct EventCellModel {
    let eventId: Int
    let eventType: String
    let location: String
    let date: String
    let isAnswered: IsAnswered
}
class EventCell: UITableViewCell {

    weak var delegate: EventCellDelegate?
    private var model: EventCellModel?
    @IBOutlet var goingButtons: [UIButton]!
    @IBOutlet var holderView: UIView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var placeLabel: UILabel!
    @IBOutlet var goingLabel: UILabel!
    @IBOutlet var eventTypeLabel: UILabel!
    @IBOutlet var eventImage: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        holderView.layer.cornerRadius = 5
    }
    
    func populate(model: EventCellModel) {
        self.model = model
        self.timeLabel.text = model.date
        self.placeLabel.text = model.location
        self.eventTypeLabel.text = model.eventType
        self.goingLabel.isHidden = !(model.isAnswered == .yes)
    }
    
    // MARK: Actions
    @IBAction func didPressGoingButton(_ sender: UIButton) {
        
        let button = goingButtons.first(where: { (button) -> Bool in
            button.tag == sender.tag
        })
        goingButtons.forEach { (button) in
            button.titleLabel?.font = UIFont.helveticaNeue(size: 17)
            button.titleLabel?.textColor = UIColor.lightGray
            button.isSelected = false
        }
        button?.titleLabel?.font = UIFont.helveticaBold(size: 17)
        button?.isSelected = true
        
        guard let model = model else {
            return 
        }
        delegate?.didSelectIsAnswered(isAnswered: sender.tag, eventId: model.eventId )
        
    }

}
