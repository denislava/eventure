//
//  BaseView.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import UIKit

/**
    Base View protocol. Associatedtype should be set in every view, implementing the protocol by setting 
    typealias ViewmodelType: <the type of ViewModel which the current view is married to>
    viewModel is the particular instance of the ViewModel, attached to the view
 */
protocol BaseView {
    associatedtype ViewModelType: BaseViewModel
    var viewModel: ViewModelType { get }
}
