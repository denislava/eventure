//
//  UIView+Xib.swift
//  CredoMobileiOS
//
//  Created by Denislava Shentova on 5/18/17.
//  Copyright © 2017 Dyanko Yovchev. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
    
    /**
     Call this method in the CustomView's class init methods.
     */
    public func xibSetup() {
        let view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "\(type(of: self))", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView ?? UIView()
        
        return view
    }
    
    /**
     Pin the current view's sides to another view.
     @params: toView - the view to be pinned to
     @params: margin - the desired margin. 0 by default
     @params: attributes: the sides to which you want to pin the view
     */
    public func pinTo(toView: UIView, margin: CGFloat = 0, attributes: NSLayoutAttribute...) {
        for side in attributes {
            let constraint = NSLayoutConstraint(item: self,
                                                attribute: side,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: toView,
                                                attribute: side,
                                                multiplier: 1.0,
                                                constant: margin)
            self.addConstraint(constraint)
        }
    }
}
