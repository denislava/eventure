//
//  LoginRequest.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

class LoginRequest: BaseRequest {
    struct Params: SerializableModel {
        let fbId: String
        let name: String
        func toJSON() -> [String: Any] {
            var params: [String: Any] = [:]
            params["fbId"] = fbId
            params["name"] = name
            return params
        }
    }
    
    init(fbId: String, name: String) {
        super.init(params: Params(fbId: fbId, name: name))
    }
}
