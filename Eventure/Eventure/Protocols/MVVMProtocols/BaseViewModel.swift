//
//  BaseViewModel.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.

/**
    Base ViewModel protocol. Associatedtype must be implemented in every ViewModel, implementing the protocol by setting 
    typealias ApiContractorType: <the type of ApiContract, if any, which the current ViewModel is married to>
    apiContractor is the particular instance of the ApiContractor, attached to the ViewModel
    init(apiContractor: ApiContractorType) is used to instantiate the particular ViewModel, 
    with typealias ApiContractType ensuring the correct type of ApiContract would be passed to the initializer
 */
protocol BaseViewModel {
    associatedtype ApiContractorType: ApiContract
    var apiContractor: ApiContractorType { get }
    init(apiContractor: ApiContractorType)

}
