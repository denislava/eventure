//
//  Globals.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import UIKit

/**
 Function to delay a certain action.
 Use example:
 delay(delayTime) {
 code to be delayed..
 }
 */
func delay(_ delay: Double, closure: @escaping () -> Void) {
    DispatchQueue
        .main
        .asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC),
                    execute: closure)
}

func divadeFloatToInts(_ value: Float) -> [Int] {
    let intArray = String(format: "%.02f", value)
        .characters
        .split(separator: ".")
        .map { Int(String($0)) ?? 0}
    return intArray
}

/// More gentle way to instantiate view controller
///
/// - Parameters:
///   - of: the ViewController.Type you'd like to instantiate
///   - from: The name of the Storyboard where the vc is located
/// - Returns: an instance of the desired ViewController
func getInstance<T: UIViewController>(of: T.Type) -> T {
    print("\(of)")
    print("\(of.storyboardName)")
    let viewController = UIStoryboard(name: of.storyboardName, bundle: nil).instantiateViewController(withIdentifier: "\(of)") as? T
    assert(viewController != nil, "No UIViewController with identifier \(of) found in Storyboard named \(T.storyboardName)")
    return viewController ?? T()
}

// MARK: - Leaving the below extension here instead of in a separate file since its logic is coupled
//         with the logic above
/// Override this propery in every ViewController which you'd like to
/// instantiate through getInstance() returning its storyboard name
/// otherwise the method's assertion will fail
extension UIViewController {
    class var storyboardName: String {
        return ""
    }
}

extension UIViewController {
    class func nibName() -> String {
        return self.nameOfClass
    }
    
    class func instantiateController<T: UIViewController>() -> T? {
        return T(nibName: self.nibName(), bundle: nil)
    }
}

@discardableResult
func roundUI(views: [UIView]) -> [UIView] {
    let new: [UIView] = views.map {
        $0.layer.cornerRadius = $0.frame.height / 2
        $0.clipsToBounds = true
        return $0
    }
    return new
}
