//
//  FormValidator.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

typealias DataTuple = (name: String, age: Int, favorites: [String: String])

// A protocol ensuring the object can be passed to the ValidatorManager instance and has the isValid and message property
protocol Validatable {
    var message: () -> String { get }
    var isValid: () -> Bool { get }
    func getBorderColor(for state: Bool) -> CGColor
    func getBackgroundColor(for state: Bool) -> UIColor
    func getBorderWidth(for state: Bool) -> CGFloat
    func setValidationRule(message: String, rule: @escaping (String) -> Bool)
}

class ValidatorManager {
    private (set) var failedValidatables: [Validatable] = []
    let shouldHighlightFailed: Bool
    
    init(highlighting: Bool = true) {
        self.shouldHighlightFailed = highlighting
    }
    
    func validate(forms: Validatable...) -> ValidationResult {
        var failureMessage = ""
        var result = true
        
        failedValidatables.removeAll()

        for form in forms {
            // Coloring the form to be validated.
            if shouldHighlightFailed {
                (form as? HighlightsOnFail)?.setValidationState(success: form.isValid())
            }
            if !form.isValid() {
                failureMessage += form.message() + "\n"
                result = false
                failedValidatables.append(form)
            }
        }
        
        let failureMessageTrimmed = failureMessage.trimmingCharacters(
            in: NSCharacterSet(charactersIn: "\n") as CharacterSet)
        
        return result ? .ok() : .fail(failureMessageTrimmed)
    }
}

enum ValidationResult {
    case ok()
    case fail(String)
}

protocol HighlightsOnFail {
    func setValidationState(success: Bool)
}

extension HighlightsOnFail where Self: Validatable {
    func setValidationState(success: Bool) {
        guard let sView = self as? UIView else { return }
        
        sView.layer.borderColor = self.getBorderColor(for: success)
        sView.backgroundColor = self.getBackgroundColor(for: success)
        sView.layer.borderWidth = self.getBorderWidth(for: success)
    }
}
