//
//  DictionaryResponse.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation

struct DictionaryResponse: DeserializableModel {
    let dictionary: [String : AnyObject]?
    
    static func fromJSON(_ jsonData: [String : AnyObject]?) -> DeserializableModel? {
        return DictionaryResponse(dictionary: jsonData)
    }
}
