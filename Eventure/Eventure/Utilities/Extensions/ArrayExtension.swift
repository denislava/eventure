//
//  ArrayExtension.swift
//  PayTalkAgent
//
//  Created by IO_MAK01 on 3/16/17.
//  Copyright © 2017 Upnetix. All rights reserved.
//

import Foundation
extension Array {
    // Safely get element by index. Returning nil instead of throwing OutOfBounds when asked for invalid index
    func get(index: Int) -> Element? {
        return self.count > index ? self[index] : nil
    }
}
