//
//  MainViewController.swift
//  Eventure
//
//  Created by Denislava on 38//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit
import FacebookCore
import FBSDKLoginKit
import SDWebImage
import MXParallaxHeader

class MainViewController: BaseViewController, BaseView {
    
    typealias ViewModelType = LoginViewModel
    var viewModel: ViewModelType = LoginViewModel(apiContractor: LoginContract())
    
    struct Constansts {
        static let headerWidth: CGFloat = 320
        static let headerMaxHeight: CGFloat = 150
        static let headerMinHeight: CGFloat = 64
        static let createButtonHeight: CGFloat = 80
        static let rowHeight: CGFloat = 120
    }
    
    override class var storyboardName: String {
        return "MainFlow"
    }
    
    fileprivate var tableData: [(image: String, label: String)] = []
    
    @IBOutlet var createButton: UIButton!
    @IBOutlet var tableView: UITableView!
    fileprivate var headerView: ProfileView?
    fileprivate var pictureUrl: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Table Data
        tableData = [("balloons", "JOINED EVENTS"), ("garland", "ALL EVENTS")]
        
        // Parallax Header
        
        headerView = ProfileView(frame: CGRect(x: 0, y: 0, width: Constansts.headerWidth, height: Constansts.headerMaxHeight))
        headerView?.delegate = self
        tableView.parallaxHeader.view = headerView // You can set the parallax header view from the floating view
        tableView.parallaxHeader.height = Constansts.headerMaxHeight
        tableView.parallaxHeader.mode = MXParallaxHeaderMode.fill
        tableView.parallaxHeader.minimumHeight = Constansts.headerMinHeight
        
        tableView.parallaxHeader.delegate = self
        
        // Header Data
        
        guard let token = AccessToken.current else {
            print("No Facebook Access Token")
            return
        }
        
        getUserName { [weak self] (name) in
            guard let pictureUrl = self?.getProfilePicture(token: token) else { return }
            self?.headerView?.populate(pictureUrl: pictureUrl, name: name, address: DataManager.local.defaultAddress)
            
            if let fbId = token.userId {
                 self?.viewModel.login(fbId: fbId, name: name)
            }
           
        }
    }
    
    // Getting Facebook Data
    
    private func getProfilePicture(token: AccessToken) -> URL? {
        
        guard let userID = token.userId else { return nil }
        return URL(string: "https://graph.facebook.com/\(userID)/picture?type=large") ?? nil
    }
    
    private func getUserName(callback: @escaping (String) -> Void) {
        let request = GraphRequest(graphPath: "me",
                                   parameters: ["fields": "name"],
                                   accessToken: AccessToken.current,
                                   httpMethod: .GET,
                                   apiVersion: FacebookCore.GraphAPIVersion.defaultVersion)
        request.start { (_, result) in
            switch result {
            case .success(let value):
                if let name = value.dictionaryValue?["name"] as? String {
                    callback(name)
                }
            case .failed(let error):
                print(error)
                callback("Unable to get user name")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController?.hidesBarsOnSwipe = false
        headerView?.setAddress(address: DataManager.local.defaultAddress)
    }
    
    @IBAction func didPressCreate(_ sender: Any) {
        let create = getInstance(of: CreateEventViewController.self)
        navigationController?.pushViewController(create, animated: true)
    }
    
}

extension MainViewController: ProfileViewDelegate {
    
    // MARK: Profile View Delegate
    
    func showEditAddress() {
        let editAddress = getInstance(of: ChangeAddressViewController.self)
        navigationController?.pushViewController(editAddress, animated: true)
    }
}

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(OptionsCell.self)") as? OptionsCell
        if let cellData = tableData.get(index: indexPath.row) {
            cell?.populate(image: cellData.image, label: cellData.label)
        }
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constansts.rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let events = getInstance(of: EventListViewController.self)
        navigationController?.pushViewController(events, animated: true)
    }
}

extension MainViewController: MXParallaxHeaderDelegate {
    
    // MARK: - Parallax header delegate
    
    func parallaxHeaderDidScroll(_ parallaxHeader: MXParallaxHeader) {
        headerView?.layoutSubviews()
    }
    
}
