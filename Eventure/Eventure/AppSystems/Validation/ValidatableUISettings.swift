//
//  ValidatableUISettings.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

class ValidatableUISettings {
    //// Default values, can be overriden for global control. Every Validatable component's UI is defaulted to these fields.
    
    struct TextFieldProperties {
        static var textColor = UIColor.black
        static var bgColor = UIColor.white
        static var placeholderColor = UIColor.gray
        static var cornerRadius: CGFloat = 3
        static var borderColor = UIColor.clear.cgColor
        static var borderWidth: CGFloat = 0
        static var font = UIFont.systemFont(ofSize: 16, weight: 0.25)
        static var tintColor = UIColor.blue
        static var failedBackground = UIColor.white
        static var failedBorderColor = UIColor.red.cgColor
        static var failedBorderWidth: CGFloat = 1
        static var autoValidate = false
    }
    
    struct TextViewProperties {
        static var bgColor = UIColor.white
        static var borderColor = UIColor.clear.cgColor
        static var borderWidth: CGFloat = 0
        static var failedBackground = UIColor.white
        static var failedBorderColor = UIColor.red.cgColor
        static var failedBorderWidth: CGFloat = 1
        static var autoValidate = false
    }
    
    struct UIViewProperties {
        static var bgColor = UIColor.white
        static var borderColor = UIColor.clear.cgColor
        static var borderWidth: CGFloat = 0
        static var failedBackground = UIColor.white
        static var failedBorderColor = UIColor.red.cgColor
        static var failedBorderWidth: CGFloat = 1
        static var autoValidate = false
    }
    
}
