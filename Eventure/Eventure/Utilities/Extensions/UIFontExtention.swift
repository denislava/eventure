//
//  UIFontExtention.swift
//  Eventure
//
//  Created by Denislava on 38//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    class func helveticaNeue(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "HelveticaNeue", size: size) ?? UIFont.systemFont(ofSize:size)
    }
    
    class func helveticaNeueThin(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "HelveticaNeue-Thin", size: size) ?? UIFont.systemFont(ofSize:size)
    }
    
    class func helveticaNeueLight(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "HelveticaNeue-Light", size: size) ?? UIFont.systemFont(ofSize:size)
    }
    
    class func helveticaBold(size: CGFloat = 18) -> UIFont {
        return UIFont(name: "HelveticaNeue-Bold", size: size) ?? UIFont.systemFont(ofSize:size)
    }
}
