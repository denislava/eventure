//
//  ApiContract.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation

/**
    Marker interface
 */
protocol ApiContract {
    
}

// TODO: Remove this dummu - here just for reference:
/*
 
 func requestProductView(productURL: String, resultCallback: @escaping (_ apiResponse: ProductViewResponse?, _ httpStatus: Int) -> Void) {
 let wrapper = RequestWrapper(request: nil,
 apiResponseClass: ProductViewResponse.self,
 relativePath: productURL,
 requestType: .get)
 
 ServerManager.sharedInstance.execute(wrapper, resultCallback: resultCallback)
 }
 
 */
