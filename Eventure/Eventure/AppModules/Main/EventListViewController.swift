//
//  EventListViewController.swift
//  Eventure
//
//  Created by Denislava on 58//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit

class EventListViewController: UIViewController {

    override class var storyboardName: String {
        return "MainFlow"
    }
    
    typealias ViewModelType = EventListViewModel
    var viewModel: ViewModelType = EventListViewModel(apiContractor: EventContract())
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        title = "Events"
        navigationController?.showBackButton()
        
        tableView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        
        viewModel.getEvents()
        viewModel.allEvents.bind { [weak self] (_) in
            self?.tableView.reloadData()
        }
    }
}

extension EventListViewController: EventCellDelegate {
    func didSelectIsAnswered(isAnswered: Int, eventId: Int) {
        viewModel.answerEvent(isAnswered: isAnswered, eventId: eventId)
    }
}

extension EventListViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.allEvents.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(EventCell.self)") as? EventCell
        //TODO: Fix event
        if let event = viewModel.allEvents.value.get(index: indexPath.row) {
            let model = EventCellModel(eventId: event.eventId, eventType: "\(event.type)", location: event.location, date: event.date, isAnswered: event.isAnswered)
            cell?.populate(model: model)
        }
        cell?.delegate = self
        cell?.selectionStyle = .none
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
}

