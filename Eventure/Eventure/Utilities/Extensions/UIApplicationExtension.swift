//
//  UIApplicationExtension.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 6/20/16.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    static var mainDelegate: AppDelegate? {
        return shared.delegate as? AppDelegate
    }
    
    func topViewController() -> UIViewController? {
        var topMostViewController: UIViewController?
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topMostViewController = topController
        }
        return topMostViewController
        
    }
    
   func showController(_ identifier: String, storyboardName: String) {
        let storyBoard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: identifier)
        
        keyWindow?.rootViewController?.presentedViewController?.dismiss(animated: false, completion: nil)
        keyWindow?.rootViewController = viewController
        keyWindow?.makeKeyAndVisible()
    }
}
