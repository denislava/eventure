//
//  DefaultPopupWrapper.swift
//  PayTalkClient
//
//  Created by dyanko.yovchev on 4/28/17.
//  Copyright © 2017 Upnetix. All rights reserved.
//

import UIKit

class DefaultPopupWrapper: ManagableAlertController {
    private let alertTitle: String
    private let message: String
    
    private var actions: [AlertAction] = []
    
    init(title: String, message: String) {
        self.alertTitle = title
        self.message = message
    }
    func addActions(actions: [AlertAction]) {
        self.actions.append(contentsOf: actions)
    }
    
    func show() {
        let alert = UIAlertController(title: alertTitle, message: message, preferredStyle: .alert)
        _ = actions.map { action in
            alert.addAction(UIAlertAction(title: action.title ?? "", callback: action.callback))
        }
        AlertManager.shared.topViewController()?.present(alert, animated: true)
    }
    
    func dismiss(onCompletion: @escaping () -> Void) {
        //...
    }
}
