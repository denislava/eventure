//
//  ValidatableTextField.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

//    A UITextField child that implements the Validable protocol and can be
//    Passed to a ValidatorManager instance. Make sure to invoke
//    setValidationRule() to every instance of this class prior to validating it
class ValidatableTextField: UITextField, Validatable, HighlightsOnFail {
    
// MARK: Validatable specific fields and methods
    var message: () -> String = {
        print("Error in ValidableTextField: invoiking isValid() while validation rule not set!")
        return ""
    }
    
    var isValid: () -> Bool = { return false }
    
    func setValidationRule(message: String, rule: @escaping (String) -> Bool) {
        self.isValid = {
            guard let text = self.text else { return false }
            return rule(text)
        }
        
        self.message = {
            return message
        }
        // Adding target for binding purposes
        self.addTarget(self, action: #selector(changed), for: .editingChanged)
    }
    
// MARK: Additional custom TextField fields and methods
    @IBInspectable var insetX: CGFloat = 5
    @IBInspectable var insetY: CGFloat = 0
    
    /// Customizable UI properties
    private (set) var textClr = ValidatableUISettings.TextFieldProperties.textColor
    private (set) var bgColor = ValidatableUISettings.TextFieldProperties.bgColor
    private (set) var placeholderClr = ValidatableUISettings.TextFieldProperties.placeholderColor
    private (set) var cornerRadius = ValidatableUISettings.TextFieldProperties.cornerRadius
    private (set) var borderColor = ValidatableUISettings.TextFieldProperties.borderColor
    private (set) var borderWidth = ValidatableUISettings.TextFieldProperties.borderWidth
    private (set) var textFont = ValidatableUISettings.TextFieldProperties.font
    private (set) var tintClr = ValidatableUISettings.TextFieldProperties.tintColor
    private (set) var failedBackground: UIColor = ValidatableUISettings.TextFieldProperties.failedBackground
    private (set) var failedBorderColor: CGColor = ValidatableUISettings.TextFieldProperties.failedBorderColor
    private (set) var failedBorderWidth: CGFloat = ValidatableUISettings.TextFieldProperties.failedBorderWidth
    
    // If set to true the field validates againts its rule on editingChanged() and changes it's appearance accordingly
    private (set) var autoValidate = ValidatableUISettings.TextFieldProperties.autoValidate
    
    private var onChange: ((String) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        applyStyles()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        applyStyles()
    }
    
    func applyStyles() {
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSForegroundColorAttributeName: placeholderClr])
        self.textColor = textClr
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        layer.borderColor = borderColor
        layer.borderWidth = borderWidth
        self.font = textFont
        self.tintColor = tintClr
    }
    
    @discardableResult
    func customize(textClr: UIColor = ValidatableUISettings.TextFieldProperties.textColor,
                   bgColor: UIColor = ValidatableUISettings.TextFieldProperties.bgColor,
                   placeholderClr: UIColor = ValidatableUISettings.TextFieldProperties.placeholderColor,
                   cornerRadius: CGFloat = ValidatableUISettings.TextFieldProperties.cornerRadius,
                   borderColor: CGColor = ValidatableUISettings.TextFieldProperties.borderColor,
                   borderWidth: CGFloat = ValidatableUISettings.TextFieldProperties.borderWidth,
                   textFont: UIFont = ValidatableUISettings.TextFieldProperties.font,
                   tintClr: UIColor = ValidatableUISettings.TextFieldProperties.tintColor,
                   failedBackground: UIColor = ValidatableUISettings.TextFieldProperties.failedBackground,
                   failedBorderColor: CGColor = ValidatableUISettings.TextFieldProperties.failedBorderColor,
                   failedBorderWidth: CGFloat = ValidatableUISettings.TextFieldProperties.failedBorderWidth,
                   autoValidate: Bool = ValidatableUISettings.TextFieldProperties.autoValidate) -> Self {
        
        self.textClr = textClr
        self.bgColor = bgColor
        self.placeholderClr = placeholderClr
        self.cornerRadius = cornerRadius
        self.borderColor = borderColor
        self.borderWidth = borderWidth
        self.textFont = textFont
        self.tintClr = tintClr
        self.failedBackground = failedBackground
        self.failedBorderColor = failedBorderColor
        self.failedBorderWidth = failedBorderWidth
        self.autoValidate = autoValidate
        
        applyStyles()
        return self
    }
    
    @discardableResult
    func bind(closure: @escaping (String) -> Void) -> Self {
        self.onChange = closure
        return self
    }
    
    func getBorderColor(for state: Bool) -> CGColor {
        return state ? borderColor : failedBorderColor
    }
    
    func getBackgroundColor(for state: Bool) -> UIColor {
        return state ? bgColor : failedBackground
    }
    
    func getBorderWidth(for state: Bool) -> CGFloat {
        return state ? borderWidth : failedBorderWidth
    }
    
    internal func changed() {
        if let text = self.text {
            if autoValidate {
                setValidationState(success: isValid())
            }
            onChange?(text)
        }
    }
    
    override func didChange(_ changeKind: NSKeyValueChange, valuesAt indexes: IndexSet, forKey key: String) {
        // TODO: Capture change and trigger event
        super.didChange(changeKind, valuesAt: indexes, forKey: key)
    }
    
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
}
