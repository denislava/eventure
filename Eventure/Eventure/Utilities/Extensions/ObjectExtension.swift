//
//  ObjectExtension.swift
//  Eventure
//
//  Created by Denislava on 28//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

public extension NSObject {
    public class var nameOfClass: String {
        guard let name = NSStringFromClass(self).components(separatedBy: ".").last else {
            return ""
        }
        return name
    }
}
