//
//  ServerMessage.swift
//  PayTalkClient
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation

enum MessageType: Int {
    case error = 0
    case success = 1
    case info = 2
}

class AbstractMessage {
    var title: String
    let message: String
    let type: MessageType
    let attributedMessage: NSMutableAttributedString?
    var audioFeedback: Bool = false
    var vibroFeedback: Bool = false

    init(type: MessageType, messagText: String, attributedText: NSMutableAttributedString? = nil) {
        self.type = type
        self.message = messagText
        self.title = type.titleForMessageType
        self.attributedMessage = attributedText
    }

    convenience init(type: MessageType, messagText: String, title: String, attributedText: NSMutableAttributedString? = nil) {
        self.init(type: type,
                  messagText: messagText,
                  attributedText: attributedText)
        self.title = title
    }
}

extension MessageType {
    var titleForMessageType: String {
        switch self {
        case .error:
            return "Error"
        case .info:
            return ""
        case .success:
            return "Success"
        }
    }
}
