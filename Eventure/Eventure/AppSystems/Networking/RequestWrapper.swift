//
//  RequestWrapper.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation

/**
    Class which acts as a wrapper of the parameters, that are to be passed to ServerManager.execute.
    Wraps all the parameters but the resultCallback closure
 */
class RequestWrapper {
    var defaultHeaders: [String: String] = [:]
    let requestData: (headers: SerializableModel?, params: SerializableModel?)
    let apiResponseClass: DeserializableModel.Type
    let relativePath: String
    let requestType: RequestMethod
    let showLoader: Bool
    private let isMultipart: Bool
    private let boundary: String = "\(NSUUID())"
    
    init(request: BaseRequest?,
         apiResponseClass: DeserializableModel.Type,
         relativePath: String,
         requestType: RequestMethod,
         acceptHeader: String = "application/json",
         contentTypeHeader: String = "application/json",
         multipart: Bool = false,
         showLoader: Bool = true) {
        
        self.requestData = (headers: request?.headers, params: request?.params)
        self.relativePath = relativePath
        self.apiResponseClass = apiResponseClass
        self.requestType = requestType
        self.showLoader = showLoader
        //headers setup
        self.defaultHeaders["Accept"] = acceptHeader
        let contentHeader = multipart ? "multipart/form-data; boundary=\(boundary)" : contentTypeHeader
        self.defaultHeaders["Content-Type"] = contentHeader
        self.isMultipart = multipart
    }
    
    func getAllHeaders() -> [String: String] {
        var headers: [String: String] = requestData.headers?.toJSON() as? [String: String] ?? [:]
        for header in defaultHeaders {
            headers[header.key] = header.value
        }
        return headers
    }
    
    var explicitBody: Data? {
        if !isMultipart {
            return nil
        }
        var body = Data()
        for (key, value) in requestData.params?.toJSON() ?? [:] {
            body.append("--\(boundary)\r\n")
            
            if let imageData = value as? Data,
                let type = imageData.contentTypeForImageData() {
                body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"imageFileName\"\r\n")
                body.append("Content-Type: \(type)\r\n\r\n")
                body.append(imageData)
                body.append("\r\n")
            } else {
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append("\(value)\r\n")
            }
        }
        
        body.append("--\(boundary)--\r\n")
        return body
    }
}
