//
//  ValidatableTextView.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

class ValidatableTextView: UITextView, Validatable, HighlightsOnFail {
    var message: () -> String = {
        print("Error in ValidableTextField: invoiking isValid() while validation rule not set!")
        return ""
    }
    
    var isValid: () -> Bool = { return false }
    
    func setValidationRule(message: String, rule: @escaping (String) -> Bool) {
        self.isValid = {
            guard let text = self.text else { return false }
            return rule(text)
        }
        
        self.message = {
            return message
        }
    }
    
    func getBorderColor(for state: Bool) -> CGColor {
        return state ? ValidatableUISettings.UIViewProperties.borderColor : ValidatableUISettings.UIViewProperties.failedBorderColor
    }
    
    func getBackgroundColor(for state: Bool) -> UIColor {
        return state ? ValidatableUISettings.UIViewProperties.bgColor : ValidatableUISettings.UIViewProperties.failedBackground
    }
    
    func getBorderWidth(for state: Bool) -> CGFloat {
        return state ? ValidatableUISettings.UIViewProperties.borderWidth : ValidatableUISettings.UIViewProperties.failedBorderWidth
    }
}
