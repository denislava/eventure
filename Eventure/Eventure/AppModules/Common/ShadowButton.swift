//
//  ShadowButton.swift
//  Eventure
//
//  Created by Denislava on 128//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit

class ShadowButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        if self.frame.size.width == super.frame.size.width {
            self.dropShadow(offsetWidth: 1, offsetHeight: -1, radius: 2)
            
        }
    }

}
