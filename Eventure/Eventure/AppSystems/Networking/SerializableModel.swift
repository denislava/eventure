//
//  ApiRequest.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation

/**
    Functional protocol defining that the instances of the implementing classes could be serialized to JSON
 */
protocol SerializableModel {
    func toJSON() -> [String: Any]
}
