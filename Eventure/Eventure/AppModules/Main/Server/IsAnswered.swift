//
//  IsAnswered.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

enum IsAnswered: Int {
    case notAnswered
    case yes
    case maybe
    case no
}
