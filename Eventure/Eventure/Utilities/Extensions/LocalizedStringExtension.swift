// Disabling the line_length lint check as this is a Strings dedicated file
// swiftlint:disable line_length

import Foundation

extension String {
    
    // MARK: GLOBAL
    static let Error = "error".localized()
    static let Success = "success".localized()
    static let Ok = "ok".localized()
    static let Alert = "alert".localized()
    static let Cancel = "cancel".localized()
    static let Done = "done".localized()
    static let Close = "close".localized()
    static let Retry = "retry".localized()
    
    // MARK: Web View
    static let OpenWeb = "open_web_string".localized()
    static let OpenCategoryWebView = "whySelectCategory".localized()
    
    // MARK: STATISTICS
    static let Statistics = "Statistics".localized()
    static let NumberOfCalls = "callsDescription".localized()
    static let NumberOfMinutes = "minutesDescription".localized()
    static let StatisticsPayoutTitle = "payoutTitle".localized()
    static let StatisticsPayoutDescription = "payoutDescription".localized()
    static let StatisticsTopHours = "topBusiestHours".localized()
    static let StatisticsCallsByLenght = "callsByLenght".localized()
    static let StatisticsDay = "day".localized()
    static let StatisticsWeek = "week".localized()
    static let StatisticsMonth = "month".localized()
    static let NoStatisticsData = "noStatisticsData".localized()
    static let CallsByLenghtChart = "callsByLenghtChart".localized()
    
    // MARK: PAYOUTS LIST
    static let Invoices = "invoices".localized()
    static let Payouts = "payouts".localized()
    static let NoInvoices = "noInvoices".localized()
    
    // MARK: SETTINGS
    static let Settings = "settings".localized()
    static let Phone = "phone".localized()
    static let Tariff = "tariff".localized()
    static let PayoutInfo = "payoutInfo".localized()
    static let Notifications = "notifications".localized()
    static let NotificationsSettings = "notificationsSettings".localized()
    static let ChangePassword = "changePass".localized()
    static let GettingStarted = "gettingStart".localized()
    static let AboutPayTalk = "about".localized()
    static let Logout = "logout".localized()
    
    // MARK: PROFILE
    static let Profile = "profile".localized()
    static let Availability = "availability".localized()
    static let AvailableDescription = "availableDescription".localized()
    static let Available = "available".localized()
    static let Offline = "offline".localized()
    static let Categories = "categories".localized()
    static let AboutMe = "aboutMe".localized()
    static let Description = "description".localized()
    static let NamePlaceholder = "namePlaceholder".localized()
    static let UsernamePlaceholder = "usernamePlaceholder".localized()
    static let CharactersLeft = "charactersLeft".localized()
    static let CategoriesDescription = "categoriesDescription".localized()
    static let ChoosePhoto = "choosePhoto".localized()
    static let TakePhoto = "takePhoto".localized()
    static let Remove = "remove".localized()
    static let YourPhotoString = "yourName".localized()
    static let NoMessages = "noMessages".localized()
    
    // MARK: PHONE NUMBER
    static let PhoneNumberDescription = "phoneNumberDescription".localized()
    
    // MARK: TARIFF
    static let TariffDescription = "tariffDescription".localized()
    static let TariffPerMinute = "tariffPerMinute".localized()
    static let TariffPerCall = "tariffPerCall".localized()
    
    // MARK: PAYOUT INFO
    static let PayoutInfoDescription = "payoutInfoDescription".localized()
    static let PayoutInfoChoose = "payoutInfoChoose".localized()
    static let PayoutInfoOwnerName = "payoutAccountOwner".localized()
    static let PayoutInfoEnterName = "payoutAccountName".localized()
    static let PayoutInfoAccountNumber = "payoutAccount".localized()
    static let Email = "email".localized()
    
    // MARK: NOTIFICATIONS
    static let NotificationsDescription = "pushNotifications".localized()
    static let NotificationsShow = "showMessage".localized()
    static let InAppNotifications = "inAppNotifications".localized()
    static let InAppVibrate = "inAppVibrate".localized()
    static let InAppSounds = "inAppSounds".localized()
    
    // MARK: CHANGE PASSWORD
    static let Password = "password".localized()
    static let RepeatPass = "repeatPass".localized()
    static let ChangePass = "changePass".localized()
    
    // MARK: ERRORS
    static let EmptyPass = "emptyPass".localized()
    static let EmptyConfirmPass = "confirmPassEmpty".localized()
    static let PasswordsDontMatch = "passwordsDontMatch".localized()
    static let PasswordCharacters = "passCharacters".localized()
    
    static let InvalidEmail = "invalidMeil".localized()
    static let NoSuchEmail = "No such email".localized()
    static let EmptyFields = "emptyFields".localized()
    static let AccountCharsCount = "accountCharsCount".localized()
    static let InvalidPhone = "invalidPhone".localized()
    static let LoginFailed = "loginFailed".localized()
    static let RegisterFailed = "registerFailed".localized()
    static let EmptyPhone = "emptyPhone".localized()
    
    //user profile
    static let InvalidUsername = "invalidUsername".localized()
    static let UsernameCharsCount = "usernameCharsCount".localized()
    static let NameCharsCount = "nameCharsCount".localized()
    static let ProfileUpdateFailed = "profileUpdateFailed".localized()
    
    static let SomethingWrong = "somethingWrong".localized()
    
    // MARK: LOGIN
    static let Login = "login".localized()
    static let ForgotPass = "forgotPass".localized()
    static let Username = "username".localized()
    static let RegisterDescription = "registerDescription".localized()
    static let CreateAccount = "createAccount".localized()
    
    // MARK: RESET PASS
    static let ResetPassword = "resetPassword".localized()
    static let ResetRassDescription = "resetPassDescription".localized()
    static let EmailPlaceholder = "emailPlaceholder".localized()
    static let ResponseMessage = ""
    
    // MARK: REGISTER
    static let RegisterAccountDescription = "registerAccountDescription".localized()
    static let SendEmailsDescription = "sendEmailsDescription".localized()

    static let RegisterButtonTitle = "registerButtonTitle".localized()
    static let TermsOfUseStart = "termsOfUseStartText".localized()
    static let TermsOfUseAnd = "termsOfUseAndText".localized()
    static let TermsOfUsePrivacy = "termsOfUsePrivacyText".localized()
    static let TermsOfUseTerms = "termsOfUseTermsText".localized()
    
    // MARK: GETTING STARTED GUIDE
    static let firstTitle           = "Become an adviser Earn money with every call"
    static let firstDescription     = "Whatever your area of expertise is, listing your services with PayTalk has many benefits. We provide you with a rich set of tools and features for you to grow your advice business."
    static let secondTitle          = "Set your call tariff and create a profile to attract callers"
    static let secondDescription    = "Create your account and complete your listing in minutes. Listed accounts get found by more callers through our website, app and major search engines."
    static let thirdTitle           = "Get paid weekly"
    static let thirdDescription     = "Payment is automatic so you never deal with invoicing or other tasks to get paid. You will receive payment via direct deposit or PayPal."
    static let fourthTitle          = "Realtime statistics of your calls and payouts"
    static let fourthDescription    = "Stay on top of your advice business with realtime call statistics and payout information. See what days and times of the day provide the best opportunities to create income."
}

protocol Localizable {
    
    func localized(language: String, args: [String]) -> Self
}

extension String: Localizable {
    static var argumentTag: String {
        return "<ARG_TAG>"
    }
    
    func localized(language: String = String.preferredLanguage, args: [String] = []) -> String {
        
        //This is the implementation when using local .strings files
        //Change this code if you're using different localization sources
        
       guard let path = Bundle.main.path(forResource: language, ofType: "lproj"),
            let bundle = Bundle(path: path) else { return self }
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
        .inject(args: args)
    }
    
     func inject(args: [String]) -> String {
        var output = self
        _ = args.map {
            if let range = output.range(of: String.argumentTag) {
                output = output.replacingCharacters(in: range, with: $0)
            }
        }
        return output
    }
    
    static var preferredLanguage: String {
        
        //LanguageCode returns the device preferred language
        //Implement you custom preference logic here
        //NSLocale.current.languageCode ?? "en"
        return "en"
    }
}
