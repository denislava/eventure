//
//  CreateEventViewController.swift
//  Eventure
//
//  Created by Denislava on 58//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit

enum PickerType {
    case type
    case date
    case location
}

enum GoingButtons: Int {
    case yes = 1
    case maybe = 2
    case no = 3
}

class CreateEventViewController: UIViewController, BaseView {
    
    override class var storyboardName: String {
        return "MainFlow"
    }
    typealias ViewModelType = CreateEventViewModel
    var viewModel: ViewModelType = CreateEventViewModel(apiContractor: EventContract())
    
    @IBOutlet var createButton: UIButton!
    @IBOutlet var eventField: UITextField!
    @IBOutlet var locationField: UITextField!
    @IBOutlet var timeField: UITextField!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var containerView: UIView!
    
    @IBOutlet var goingButtons: [UIButton]!
    private var datePicker = UIDatePicker()
    fileprivate var typePicker = UIPickerView()
    
    //server
    fileprivate var dateForServer: String?
    fileprivate var isAnswered: IsAnswered?
    fileprivate var eventType: Int?
    
    //TODO: Testing
    fileprivate var typeOptions = ["Листчета", "Секс"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        title = "Create New Event"
        navigationController?.showBackButton()
        
        ScrollKeyboardAdjuster.shared.startObserving(scrollView, holderView: self.view)
        
        configureDatePicker()
        configureTypePicker()
        configureLocationField()
    }
    
    private func configureDatePicker() {
        
        datePicker.datePickerMode = .dateAndTime
        datePicker.minuteInterval = 15
        datePicker.minimumDate = Date()
        // add toolbar to textField
        timeField.inputAccessoryView = createPickerToolBar(pickerType: .date)
        // add datepicker to textField
        timeField.inputView = datePicker
    }
    
    private func configureTypePicker() {
        typePicker.delegate = self
        typePicker.dataSource = self
        eventField.inputView = typePicker
        eventField.inputAccessoryView = createPickerToolBar(pickerType: .type)
    }
    
    private func configureLocationField() {
        locationField.inputAccessoryView = createPickerToolBar(pickerType: .location)
        if let defaultAddress = DataManager.local.defaultAddress {
            locationField.text = defaultAddress
        }
    }
    
    func datePickerDone() {
        let formatter = DateFormatter()
        formatter.dateFormat = DateFormat.appCommon
        timeField.text = formatter.string(from: datePicker.date)
        formatter.dateFormat = DateFormat.serverFormat
        dateForServer = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        view.endEditing(true)
    }
    
    func typePickerDone() {
        if eventField.text?.isEmpty ?? false {
            eventField.text = typeOptions.get(index: 0)
            eventType = 1 // TODO: Refactor
        }
        view.endEditing(true)
    }
    
    func pickerCancel() {
        view.endEditing(true)
    }
    
    private func createPickerToolBar(pickerType: PickerType) -> UIToolbar {
        let selector: Selector
        switch pickerType {
        case .date:
            selector = #selector(datePickerDone)
        case .location:
            selector = #selector(pickerCancel)
        case .type:
            selector = #selector(typePickerDone)
        }
        
        return DoneCancelToolbar.create(target: self, doneSelector: selector, cancelSelector: #selector(pickerCancel))
    }
    
    @IBAction func didPressGoingButton(_ sender: UIButton) {
        
        let button = goingButtons.first(where: { (button) -> Bool in
            button.tag == sender.tag
        })
        goingButtons.forEach { (button) in
            button.titleLabel?.font = UIFont.helveticaNeue(size: 17)
            button.titleLabel?.textColor = UIColor.lightGray
            button.isSelected = false
        }
        button?.titleLabel?.font = UIFont.helveticaBold(size: 17)
        button?.isSelected = true
        isAnswered = IsAnswered(rawValue: sender.tag)
        
    }
    
    @IBAction func didPressCreateButton(_ sender: Any) {
        guard verifyInput() else { return }
        
        guard let type = eventType,
            let date = dateForServer,
            let location = locationField.text,
            let answer = isAnswered else {
                return
        }
        viewModel.createEvent(type: type, date: date, location: location, isAnswered: answer.rawValue)
    }
    
    private func verifyInput() -> Bool {
        var isValid = true
        var message = ""
        if eventType == nil {
            message = "Choose event type!"
            isValid = false
        } else if locationField.text?.isEmpty ?? true {
            message = "Choose location!"
            isValid = false
        } else if dateForServer == nil {
            message = "Choose date!"
            isValid = false
        } else if isAnswered == nil {
            message = "Are you going?"
            isValid = false
        }
        if !isValid {
            let message = AbstractMessage(type: .error, messagText: message)
            AlertManager.shared.showSystem(message)
        }
        return isValid
    }
}

extension CreateEventViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return typeOptions.get(index: row) ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        eventField.text = typeOptions.get(index: row)
        eventType = 1 // TODO: FIX
    }
}

extension CreateEventViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        ScrollKeyboardAdjuster.shared.focusedView = textField
        if textField == eventField {
            textField.text = ""
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == locationField && textField.text == DataManager.local.defaultAddress {
            textField.text = ""
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
