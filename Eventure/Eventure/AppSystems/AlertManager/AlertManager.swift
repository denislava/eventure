//
//  AlertManager.swift
//  PayTalkClient
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

/// Classes implementing this interface should be able to show popup messages
protocol CallableAlertManager {
    func showNotification(_ messageObject: AbstractMessage, action: AlertAction?)
    func showCustom(_ messageObject: AbstractMessage, actions: [AlertAction]?)
    func showSystem(_ messageObject: AbstractMessage, actions: [AlertAction]?)
    func showToast(_ messageObject: AbstractMessage, action: AlertAction?)
}

protocol ManagableAlertController {
    func show()
    func addActions(actions: [AlertAction])
    func dismiss(onCompletion: @escaping () -> Void)
}

/// Delegate receiving callbacks on certain system-importaint AlertManager events.
/// Useful when ActivityIndicator/Loader is present
protocol AlertManagerDelegate {
    
    /// Invoked when an alert is about to be shown by the AlertManager instance
    func alertWillShow()
}

extension ManagableAlertController where Self: UIViewController {
    func dismiss(onCompletion: @escaping () -> Void) {
        dismiss(animated: false) {
            onCompletion()
            AlertManager.shared.popFirstAlert()
        }
    }
    
    // Since the showing is not animated here, It's importaint to implement a nice animation/transition
    /// on viewDidAppear() in your custom alert controller, implementing ManagableAlertController
    /// in case any new is introduced.
    func show() {
        AlertManager.shared.topViewController()?.present(self, animated: false, completion: nil)
    }
}

typealias AlertViewTextInputHandler = ((UITextField) -> Void)

class AlertManager: CallableAlertManager {
    private static var instance: AlertManager?
    //swiftlint:disable weak_delegate
    private let delegate: AlertManagerDelegate?
    //swiftlint:enable weak_delegate
    private var alertsQueue: [ManagableAlertController] = []
    
    private init(delegate: AlertManagerDelegate? = nil) {
        self.delegate = delegate
    }
    
    static var shared: AlertManager {
        assert(instance != nil, "Trying to access AlertManager.shared before invoking AlertManager.instantiate()")
        return instance ?? AlertManager()
    }
    
    /// Entry point for the AlertManager.shared instance
    ///
    /// - Parameter delegate: Optional AlertManagerDelegate instace to receive callbacks
    static func instantiate(delegate: AlertManagerDelegate? = nil) {
        AlertManager.instance = AlertManager(delegate: delegate)
    }
    
    /// Shows a notification-style view on top of screen, which does not block the UI and hides on tap
    ///
    /// - Parameters:
    ///   - messageObject: an AbstractMessage object
    ///   - action: Optional AlertAction to be executed on eventual button tap
    func showNotification(_ messageObject: AbstractMessage, action: AlertAction? = nil) {
        let alertToast = InAppNotificationView.getView()
        
        var actionsToAdd: [AlertAction] = []
        if let act = action {
            actionsToAdd.append(act)
        }
        alertToast.addActions(actions: actionsToAdd)
        alertToast.shouldBeep = messageObject.audioFeedback
        alertToast.shouldVibrate = messageObject.vibroFeedback
        alertToast.notificationTextLabel.text = messageObject.message
        addToQueue(alertToast)
    }
    
    /// Shows a custom style Popup which blocks the UI
    ///
    /// - Parameters:
    ///   - messageObject: an AbstractMessage object
    ///   - actions: Array of AlertAction objects to be presented in the popup
    func showCustom(_ messageObject: AbstractMessage, actions: [AlertAction]? = nil) {
        let unwrappedActions = actions ?? []
        self.showAlertController(messageObject, actions: unwrappedActions, style: .alert)
    }
    
    /// Shows a system style Popup
    ///
    /// - Parameters:
    ///   - messageObject: an AbstractMessage object
    ///   - actions: Array of AlertAction objects to be presented in the popup
    func showSystem(_ messageObject: AbstractMessage, actions: [AlertAction]? = nil) {
        var actionsToAppend = actions ?? []
        if actionsToAppend.isEmpty {
            actionsToAppend.append(AlertAction(title: "OK", callback: {}))
        }
        
        let alertView = DefaultPopupWrapper(title: messageObject.title, message: messageObject.message)
        
        alertView.addActions(actions: actionsToAppend)
        addToQueue(alertView)
    }
    
    /// Shows an Android-like toast which does not block the UI
    ///
    /// - Parameters:
    ///   - messageObject: an AbstractMessage object
    ///   - action: Optional AlertAction to be executed on eventual tap on the view
    func showToast(_ messageObject: AbstractMessage, action: AlertAction? = nil) {
        // TODO: Implement toast-like view
    }

    /// Shows Popup with UITextField for input
    ///
    /// - Parameters:
    ///   - messageObject: an AbstractMessage object
    ///   - textFieldHandler: Handler to handle the text input
    ///   - callback: Callback to be executed on "OK" tap
    func showAlertWithTextField(_ messageObject: AbstractMessage, textFieldHandler: ((UITextField) -> Void)?, callback: @escaping () -> Void) {
        let confirmationAction: AlertAction = AlertAction(title: "OK", callback: callback)
        self.showAlertController(messageObject, actions: [confirmationAction], style: .alert, textFieldHandler: textFieldHandler)
    }
    
    private func showAlertController(_ messageObject: AbstractMessage,
                                     actions: [AlertAction],
                                     style: UIAlertControllerStyle,
                                     textFieldHandler: AlertViewTextInputHandler? = nil) {
        var actionsToAppend = actions
        guard
            let alertView = UIStoryboard.init(name: "Alert", bundle: nil)
                                        .instantiateViewController(withIdentifier: "\(BasicAlertController.self)") as? BasicAlertController else { return }
        alertView.modalPresentationStyle = .overCurrentContext
        alertView.setup(title: messageObject.title, message: messageObject.message, attributedMessage: messageObject.attributedMessage)
        
        // TODO: Fix custom Alert class to handle textField enabling
        if let handler = textFieldHandler {
            alertView.addTextField(configurationHandler: handler)
            actionsToAppend.append(AlertAction(title: "Cancel", callback: {}))
        }
        // Checking if no actions are passed to AlertManager and adding default popup-close action with button labeled "OK"
        if actionsToAppend.isEmpty {
            actionsToAppend.append(AlertAction(title: "OK", callback: {}))
        }
        alertView.addActions(actions: actionsToAppend)
        addToQueue(alertView)
    }
    
    func popFirstAlert() {
        alertsQueue.removeFirst()
        guard let nextAlert = alertsQueue.first else { return }
        show(nextAlert)
    }
    
    private func addToQueue(_ alert: ManagableAlertController) {
        if alertsQueue.isEmpty {
            show(alert)
        }
        alertsQueue.append(alert)
    }
    
    private func show(_ alert: ManagableAlertController) {
        delegate?.alertWillShow()
        alert.show()
    }
    
    func topViewController() -> UIViewController? {
        var topMostViewController: UIViewController?
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topMostViewController = topController
        }
        return topMostViewController
        
    }
}

extension UIAlertAction {
    convenience init(title: String, style: UIAlertActionStyle = .default, callback: @escaping () -> Void) {
        self.init(title: title, style: style, handler: { action in
            _ = action
            callback()
            AlertManager.shared.popFirstAlert()
        })
    }
}
