//
//  CreateEventViewModel.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

class CreateEventViewModel: BaseViewModel {
    
    typealias ApiContractorType = EventContract
    var apiContractor: ApiContractorType = EventContract()
    required init(apiContractor: ApiContractorType) {
        self.apiContractor = apiContractor
    }
    
    func createEvent(type: Int, date: String, location: String, isAnswered: Int) {
        apiContractor.createEvent(type: type, date: date, location: location, isAnswered: isAnswered) { [weak self] (response, _) in
            guard self != nil,
                response != nil else { return }
            let message = AbstractMessage(type: .success, messagText: "Event successfully created!")
            AlertManager.shared.showSystem(message)
        }
    }
}
