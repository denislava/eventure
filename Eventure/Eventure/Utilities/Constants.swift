/// -------------- ///
/// APP CONSTANTS  ///
/// -------------- ///

struct Server {
    static let baseUrl = "http://192.168.0.102:5000"
}
struct UIConstants {
    
}

struct DateFormat {
    static let appCommon = "H:mm - dd MMM, EEEE"
    static let serverFormat = "yyyy-MM-dd HH:mm:ss"
}

struct UserDefaultsConstants {
    static let defaultAddressKey = "defaultAddressKey"
}
