//
//  ProfileView.swift
//  Eventure
//
//  Created by Denislava on 38//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit
import SDWebImage

protocol ProfileViewDelegate: class {
    func showEditAddress()
}

class ProfileView: UIView {

    @IBOutlet private var picture: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var addressLabel: UILabel!
    
    weak var delegate: ProfileViewDelegate?
    
    func populate(pictureUrl: URL, name: String, address: String?) {
        picture.sd_setImage(with: pictureUrl)
        nameLabel.text = name
        setAddress(address: address)
        picture.clipsToBounds = true
        layoutSubviews()
    }
    
    func setAddress(address: String?) {
        if let address = address {
            addressLabel.text = address
        }
        layoutSubviews()
    }
    
    @IBAction func didPressEditAddress(_ sender: Any) {
        delegate?.showEditAddress()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        picture.layer.cornerRadius = picture.bounds.height / 2
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
}
