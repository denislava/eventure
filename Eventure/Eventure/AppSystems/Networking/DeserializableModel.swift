//
//  ApiResponse.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation

/**
    Functional protocol defining that the instances of the implementing classes could be initialized by deserializing from a passed JSON
 */
protocol DeserializableModel {
    static func fromJSON(_ jsonData: [String: AnyObject]?) -> DeserializableModel?
}

extension DeserializableModel {
    
    /**
        Method for building arrays of DeserializableModels from given array of Dictionaries by calling T.fromJSON(). 
        Method uses Generics and the class type should be passed to work properly
    */
    static func buildArray(_ jsonData: [[String : AnyObject]]?) -> [Self] {
        var array: [Self] = []
        guard let jsonData = jsonData else { return array }
        
        for tData in jsonData {
            guard let tElement = Self.fromJSON(tData) as? Self else { continue }
            array.append(tElement)
        }
        return array
    }
}
