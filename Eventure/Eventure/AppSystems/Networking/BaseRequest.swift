//
//  BaseRequest.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

/**
    Base class defining that the instance of the implementing classes have properties for request Headers and request Params, 
    which can on their turn be serialized to dictionaries
 */
class BaseRequest {
    let headers: SerializableModel?
    let params: SerializableModel?
    
    init(headers: SerializableModel? = nil, params: SerializableModel? = nil) {
        self.headers = headers
        self.params = params
    }
}
