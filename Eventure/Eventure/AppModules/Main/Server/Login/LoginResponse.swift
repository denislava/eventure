//
//  LoginResponse.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

struct LoginResponse: DeserializableModel {
    
    let userId: Int
    let eventTypes: [EventType]
    static func fromJSON(_ jsonData: [String : AnyObject]?) -> DeserializableModel? {
        guard let data = jsonData,
        let userId = data["usersId"] as? Int
            else { return nil }
        let eventTypes = EventType.buildArray(data["eventTypes"] as? [[String : AnyObject]])
        return LoginResponse(userId: userId, eventTypes: eventTypes)
    }
}

struct EventType: DeserializableModel {

    let id: Int
    let name: String
    
    static func fromJSON(_ jsonData: [String : AnyObject]?) -> DeserializableModel? {
        guard let data = jsonData,
        let id = data["eventTypesId"] as? Int,
            let name = data["name"] as? String else { return nil }
        return EventType(id: id, name: name)
    }
}
