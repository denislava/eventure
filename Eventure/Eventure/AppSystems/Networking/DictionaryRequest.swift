//
//  DictionaryRequest.swift
//  PayTalkAgent
//
//  Created by IO_MAK01 on 3/30/17.
//  Copyright © 2017 Upnetix. All rights reserved.
//

import Foundation

class DictionaryRequest: BaseRequest {
    struct HeadersOrParams: SerializableModel {
        let dictionary: [String: Any]
        
        func toJSON() -> [String : Any] {
            return dictionary
        }
    }
    
    init(dictionary: [String: Any], asHeaders: Bool = false) {
        let headers = asHeaders ? HeadersOrParams(dictionary: dictionary) : nil
        let params = !asHeaders ? HeadersOrParams(dictionary: dictionary) : nil
        super.init(headers: headers, params: params)
    }
}
