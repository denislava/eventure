//
//  DoneCancelToolbar.swift
//  Eventure
//
//  Created by Denislava on 128//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit

class DoneCancelToolbar: UIToolbar {

    class func create(target: Any, doneSelector: Selector, cancelSelector: Selector) -> UIToolbar {
        //ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: target, action: doneSelector)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: target, action:  cancelSelector)
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        return toolbar

    }
}
