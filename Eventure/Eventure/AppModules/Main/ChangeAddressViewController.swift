//
//  ChangeAddressViewController.swift
//  Eventure
//
//  Created by Denislava on 128//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit

class ChangeAddressViewController: UIViewController {

    fileprivate let addressPlaceholder = "address"
    
    override class var storyboardName: String {
        return "MainFlow"
    }
    
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var addressTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        title = "Default Location"
        navigationController?.showBackButton()
        
        if let address = DataManager.local.defaultAddress {
            addressTextView.text = address
        } else {
            addressTextView.text = addressPlaceholder
        }
        addressTextView.inputAccessoryView = DoneCancelToolbar.create(target: self, doneSelector: #selector(pickerCancel), cancelSelector: #selector(pickerCancel))
    }
    
    func pickerCancel() {
        view.endEditing(true)
    }
    
    @IBAction func didPressSave(_ sender: Any) {
        if let address = addressTextView.text {
            DataManager.local.saveDefaultAddress(address: address)
        }
        navigationController?.popViewController(animated: true)
    }
}

extension ChangeAddressViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == addressPlaceholder {
            textView.text = ""
        }
    }
}
