//
//  RequestsTracker.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation

class RequestsTracker: NetworkerLoggingDelegate {
    private var waitingList: [String: [String: Any]] = [:]
    
    func logRequest(requestUID: String, request: [String : Any]) {
        // Saving the request to be matched and printed together with corresponding response on logResponse()
        waitingList[requestUID] = request
    }
    
    func logResponse(responseUID: String, timestamp: TimeInterval, data: Data?, response: URLResponse?, error: Error?) {
        
        // Getting the request corresponding to the received response
        guard let request = waitingList[responseUID] else {
            print("NetworkerDelegate Error: No request matching ID \(responseUID) found.")
            return
        }
        var params: [String: AnyObject]?
        guard let data = data else { return }
        do {
            params = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String: AnyObject]
            if params == nil,
                let arrParams = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [[String: AnyObject]] {
                params = [:]
                params?["rootArray"] = arrParams as AnyObject?
            }
        } catch {
            print("NetworkerDelegate Error: JSON parsing failed with error message:", "\(error)")
        }
        
        // So we get rid of that annoying "Optional()" in the string below
        let unwrappedParams = "\(params ?? [:])"
        let unwrappedResponse: String
        if let unwrapped = response {
            unwrappedResponse = "\(unwrapped)"
        } else {
            unwrappedResponse = "nil"
        }
        
        print(buildLog(request: request, responseUID: responseUID, timestamp: timestamp, unwrappedResponse: unwrappedResponse, unwrappedParams: unwrappedParams))
    }
    
    private func buildLog(request: [String: Any], responseUID: String, timestamp: TimeInterval, unwrappedResponse: String, unwrappedParams: String) -> String {
        var log = ""
        log.append("\n|=================================================================\n")
        log.append("|============================ REQUEST ============================\n")
        log.append("|URL:               \(request["URL"] ?? "")\n")
        log.append("|method:            \(request["method"] ?? "")\n")
        log.append("|headers:           \(request["headers"] ?? "")\n")
        log.append("|body:              \(request["body"] ?? "")\n")
        log.append("|\n")
        log.append("|timestamp:         \(request["timestamp"] ?? "")\n")
        log.append("|Unique ID:         \(request["requestUID"] ?? "")\n")
        log.append("|\n")
        log.append("|\n")
        log.append("|----------------------------- RESPONSE -----------------------------\n")
        log.append("|Unique ID:         \(responseUID)\n")
        log.append("|timestamp:         \(timestamp)\n")
        log.append("|query duration     \(timestamp - (request["timestamp"] as? TimeInterval ?? 0))sec\n")
        log.append("|response fields:\n")
        log.append("|\(unwrappedResponse)\n")
        log.append("|\n")
        log.append("|\n")
        log.append("|\n")
        log.append("|\n")
        log.append("|response body /JSON/:\n")
        log.append("|\(unwrappedParams)\n")
        log.append("|==================================================================\n")
        log.append("|==================================================================\n\n")
        
        return log
    }
    
}
