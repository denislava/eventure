//
//  Log.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

class Log {
    static var enabled = true
    static private var allowedTags: [String] = []
    static private var bannedTags: [String] = []
    
    /**
        Use to add message with tag to be printed if the tag is allowed and logging is enabled
    */
    static func put(tag: String, messages: String...) {
        if enabled && (allowedTags.isEmpty || allowedTags.contains(tag)) && !bannedTags.contains(tag) {
            print("\n| ------------------------------------------- \(tag) ------------------------------------------- | ")
            for message in messages {
                print("| \(message)")
            }
            print("|------------------------------------------------------------------------------------------------|\n\n")
        }
    }
    
    /**
        Use to set a var args of Tags to be printed. All other tags would be neglected.
    */
    static func showOnly(tags: String...) {
        for tag in tags {
            allowedTags.append(tag)
        }
    }
    
    /**
        Use to set a var args of Tags to be excluded from the print.
    */
    static func exclude(tags: String...) {
        for tag in tags {
            bannedTags.append(tag)
        }
    }
    
}
