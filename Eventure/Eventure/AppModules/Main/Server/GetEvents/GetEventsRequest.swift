//
//  GetEventsRequest.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

class GetEventsRequest: BaseRequest {
    struct Params: SerializableModel {
        let userId: Int
        func toJSON() -> [String: Any] {
            var params: [String: Any] = [:]
            params["usersId"] = userId
            return params
        }
    }
    
    init(userId: Int) {
        super.init(params: Params(userId: userId))
    }
}
