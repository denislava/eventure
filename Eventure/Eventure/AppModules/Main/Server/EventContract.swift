//
//  EventContract.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

class EventContract: ApiContract {
    
    func createEvent(type: Int, date: String, location: String, isAnswered: Int, resultCallback: @escaping (_ apiResponse: EventResponse?, _ httpStatus: Int) -> Void) {
        
        //TODO: USer ID
        let relativePath = "/createEvent"
        let request = CreateEventRequest(userId: 1, type: type, date: date, location: location, isAnswered: isAnswered)
        let wrapper = RequestWrapper(request: request,
                                     apiResponseClass: EventResponse.self,
                                     relativePath: relativePath,
                                     requestType: .post)
        ServerManager.sharedInstance.execute(wrapper, resultCallback: resultCallback)
        
    }
    
    func answerEvent(eventId: Int, isAnswered: Int, resultCallback: @escaping (_ apiResponse: StatusResponse?, _ httpStatus: Int) -> Void) {
        
        let relativePath = "/answerEvent"
        let request = AnswerEventRequest(userId: 1, eventId: eventId, isAnswered: isAnswered)
        let wrapper = RequestWrapper(request: request,
                                     apiResponseClass: StatusResponse.self,
                                     relativePath: relativePath,
                                     requestType: .post)
        ServerManager.sharedInstance.execute(wrapper, resultCallback: resultCallback)
        
    }
    
    func getEvents(resultCallback: @escaping (_ apiResponse: GetEventsResponse?, _ httpStatus: Int) -> Void) {
        
        let relativePath = "/getEvents"
        let request = GetEventsRequest(userId: 1)
        let wrapper = RequestWrapper(request: request,
                                     apiResponseClass: GetEventsResponse.self,
                                     relativePath: relativePath,
                                     requestType: .get)
        ServerManager.sharedInstance.execute(wrapper, resultCallback: resultCallback)
        
    }
}
