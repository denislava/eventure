//
//  LoginViewController.swift
//  Eventure
//
//  Created by Denislava on 28//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

class LoginViewController: BaseViewController {
    
    struct Constants {
        static let zeroAlpha: CGFloat = 0
        static let normalAlpha: CGFloat = 1
        static let visibleImageTop: CGFloat = 8
        static let imageAnimationDuration: TimeInterval = 2
        static let springDamping: CGFloat = 0.45
        static let springVelocity: CGFloat = 5
        static let blinkDuration: TimeInterval = 1
        static let blinkDelay: Double = Constants.blinkDuration * 4
    }
    
    override class var storyboardName: String {
        return "MainFlow"
    }
    
    @IBOutlet var peopleImage: UIImageView!
    @IBOutlet var imageTopConstraint: NSLayoutConstraint!
    @IBOutlet var joinLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        joinLabel.alpha = Constants.zeroAlpha
        peopleImage.alpha = Constants.zeroAlpha
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.peopleImage.alpha = Constants.normalAlpha
        self.imageTopConstraint.constant = Constants.visibleImageTop
        UIView.animate(withDuration: Constants.imageAnimationDuration,
                       delay: 0,
                       usingSpringWithDamping: Constants.springDamping,
                       initialSpringVelocity: Constants.springVelocity,
                       options: .curveEaseIn,
                       animations: { [weak self] in
            self?.view.layoutIfNeeded()
        }, completion: nil)
        
        joinLabel.startBlink(duration: Constants.blinkDuration)
        delay(Constants.blinkDelay) {
            self.joinLabel.stopBlink()
        }
    }
    @IBAction func didPressLogin(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile ], viewController: self) { [weak self] loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success:
                self?.goToMainScreen()
            }
        }
    }
    
    private func goToMainScreen() {
        self.dismiss(animated: true) { [presentingViewController] in
            if let splashVC = (presentingViewController as? UINavigationController)?.topViewController as? SplashViewController {
                splashVC.showMainScreen()
            }
        }
    }
}
