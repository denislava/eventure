//
//  ScrollKeyboardAdjuster.swift
//  Test
//
//  Created by Denislava Shentova on 4/19/17.
//  Copyright © 2017 Denislava Shentova. All rights reserved.
//

import UIKit

class ScrollKeyboardAdjuster: NSObject {
    static let shared = ScrollKeyboardAdjuster()
    
    fileprivate(set) weak var scrollView: UIScrollView?
    fileprivate(set) weak var holderView: UIView!
    weak var focusedView: UIView?
    
    fileprivate var originalScrollOffset: CGPoint?
    fileprivate var originalScrollSize: CGSize?
    
    var isKeyboardShown: Bool = false
    
    func startObserving(_ scrollView: UIScrollView, holderView: UIView) {
        
        self.scrollView = scrollView
        self.holderView = holderView
        
        //remove old observers
        stopObserving()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func stopObserving() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: scrollview adjust when keyboard present
    @objc fileprivate func keyboardWillShow(_ notification: Notification) {
        
        guard self.holderView != nil && self.holderView.window != nil else {
            return
        }
        
        if let scrollView = scrollView,
            let userInfo = (notification as NSNotification).userInfo,
            let keyboardFrameInWindow = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue,
            let keyboardFrameInWindowBegin = userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue,
            let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber,
            let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {

            // the keyboard frame is specified in window-level coordinates. 
            //this calculates the frame as if it were a subview of our view, making it a sibling of the scroll view

            let keyboardFrameInView = self.holderView.convert(keyboardFrameInWindow.cgRectValue, from: nil)
            
            let scrollViewKeyboardIntersection = scrollView.frame.intersection(keyboardFrameInView)
            
            UIView.animate(withDuration: animationDuration.doubleValue,
                           delay: 0,
                           options: UIViewAnimationOptions(rawValue: UInt(animationCurve.uintValue)),
                           animations: { [weak self] in
                            
                            if let strongSelf = self, let scrollView = strongSelf.scrollView {
                                if let focusedControl = strongSelf.focusedView {
                                    // if the control is a deep in the hierarchy below the scroll view, this will calculate the frame as if it were a direct subview
                                    var controlFrameInScrollView = scrollView.convert(focusedControl.bounds, from: focusedControl)
                                    controlFrameInScrollView = controlFrameInScrollView.insetBy(dx: 0, dy: -10)
                                    
                                    let controlVisualOffsetToTopOfScrollview = controlFrameInScrollView.origin.y - scrollView.contentOffset.y
                                    let controlVisualBottom = controlVisualOffsetToTopOfScrollview + controlFrameInScrollView.size.height
                                    
                                    // this is the visible part of the scroll view that is not hidden by the keyboard
                                    let scrollViewVisibleHeight = scrollView.frame.size.height - scrollViewKeyboardIntersection.size.height
                                    
                                    var newContentOffset = scrollView.contentOffset
                                    //store it to better update latter
                                    strongSelf.originalScrollOffset = newContentOffset
                                    
                                    if controlVisualBottom > scrollViewVisibleHeight { // check if the keyboard will hide the control in question
                                        newContentOffset.y += (controlVisualBottom - scrollViewVisibleHeight)
                                        
                                        //check for impossible offset
                                        newContentOffset.y = min(newContentOffset.y, scrollView.contentSize.height - scrollViewVisibleHeight)
                                    } else if controlFrameInScrollView.origin.y < scrollView.contentOffset.y { // if the control is not fully visible, make it so 
                                        //(useful if the user taps on a partially visible input field

                                        newContentOffset.y = controlFrameInScrollView.origin.y
                                    }
                                    
                                    //no animation as we had own animation already going
                                    scrollView.setContentOffset(newContentOffset, animated: false)
                                }
                                
                                var scrollSize = scrollView.contentSize
                                if strongSelf.originalScrollSize != nil {
                                    //subtract old keyboard value
                                    scrollSize.height -= strongSelf.holderView.convert(keyboardFrameInWindowBegin.cgRectValue, from: nil).size.height
                                } else {
                                    strongSelf.originalScrollSize = scrollSize
                                }
                                
                                scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: scrollSize.height + keyboardFrameInView.height)
                            }
                            
                },
                           completion: nil)
        }
        
        isKeyboardShown = true
    }
    
    @objc fileprivate func keyboardWillHide(_ notification: Notification) {
        
        guard self.holderView != nil && self.holderView.window != nil else {
            return
        }
        
        if scrollView != nil,
            let userInfo = (notification as NSNotification).userInfo,
            let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber,
            let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
            UIView.animate(withDuration: animationDuration.doubleValue,
                           delay: 0,
                           options: UIViewAnimationOptions(rawValue: UInt(animationCurve.uintValue)),
                           animations: {
                            
                            if let scrollView = self.scrollView {
                                if let originalContentSize = self.originalScrollSize {
                                    scrollView.contentSize = originalContentSize
                                }
                                if let originalScrollOffset = self.originalScrollOffset {
                                    scrollView.setContentOffset(originalScrollOffset, animated: false)
                                }
                            }
            },
                           completion: { _ in
                            self.originalScrollOffset = nil
                            self.originalScrollSize = nil

            })
        }
        
        isKeyboardShown = false
    }
    
}
