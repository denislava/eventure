//
//  ServerManager.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

class ServerManager {
    private static let instance = ServerManager()
    private var alertManager: CallableAlertManager!
    private (set) var networker: Networker!
    
    /**
     Headers for the requests
     */
    fileprivate let appOs = "iOS"
    fileprivate var deviceID: String {
        return UIDevice.current.identifierForVendor?.uuidString ?? "not_available"
    }
    fileprivate var appVersion: String {
        guard let dict = Bundle.main.infoDictionary,
            let version = dict["CFBundleShortVersionString"] as? String,
            let build = dict["CFBundleVersion"] as? String else { return "not_available" }
        return "\(version)\(build)".replacingOccurrences(of: ".", with: "")
    }
    
    /**
     Gets an instance of the ServerManager class. Makes assertion whether the alertManager is instantiated before returning the instance
     */
    static var sharedInstance: ServerManager {
        assert(instance.alertManager != nil, "AlertManager instance not set")
        assert(instance.networker != nil, "Networker instance not set")
        return instance
    }
    
    /**
     Static method used to setup the class with the necessary fields/instances.
     Must be invoked before the first call to sharedInstance otherwise the assertions in sharedInstance will fail
     */
    static func instantiate(networker: Networker, alertManager: CallableAlertManager, baseURL: String) {
        instance.alertManager = alertManager
        instance.baseURL = baseURL
        instance.networker = networker
    }
    
    fileprivate var baseURL = ""
}

extension ServerManager: ServerManagerProtocol {
    
    func execute<T: DeserializableModel>(_ wrapper: RequestWrapper,
                 dataProcessor: JsonProcessor = JsonProcessor(),
                 resultCallback: @escaping (_ apiResponse: T?, _ httpStatus: Int) -> Void) {
        
        let processor = dataProcessor
        
        var headers: [String: String] = wrapper.getAllHeaders()
        let params = wrapper.requestData.params?.toJSON() ?? [:]
        let path = baseURL + wrapper.relativePath
        let method = wrapper.requestType
        
        // Appending app-scope headers if available:
        // NOTE: The content-type and Accept headers are now defaulted to
        // application/json and come from the RequestWrapper to give the
        // ability to override them for specific requests
        headers["osVersion"] = appOs
        headers["appVersion"] = appVersion
        
        networker.request(fullPath: path,
                          method: method,
                          parameters: params,
                          headers: headers,
                          explicitBody: wrapper.explicitBody) { [weak self] data, rawResponse, error in
                            
                            let response = rawResponse as? HTTPURLResponse
                            let statusCode = response?.statusCode ?? 0
                            let incHeaders = response?.allHeaderFields
                            // Declare the objects needed to call resultCallback:
                            var apiResponse: T?
                            
                            // Processing the Data? from the response and creating apiResponse of type T: DeserializableModel
                            var parameters: [String: AnyObject] = [:]
                            
                            switch processor.process(data: data) {
                            case .OK(let params):
                                parameters = params
                            case .failed(let description, let system, let data):
                                Log.put(tag: "ServerManager",
                                        messages: description, system, data)
                            }
                            apiResponse = processor.cast(incommingHeaders: incHeaders, processedParams: parameters, to: T.self)
                            
                            // Checking if status says server error and showing popup if so:
                            if statusCode > 499 || statusCode == 0 {
                                let ok = AlertAction(title: .Close) {
                                    resultCallback(apiResponse, statusCode)
                                }
                                let retry = AlertAction(title: .Retry) {
                                    self?.execute(wrapper, resultCallback: resultCallback)
                                }
                                let msgText = statusCode == 0 ? "You've lost your internet connection" : "Server error"
                                AlertManager.shared.showSystem(AbstractMessage(type: .error, messagText: msgText), actions: [ok, retry])
                            } else {
                                
                                if let serverMsg = ((parameters["result"] as? [String: AnyObject])?["error"] as? [String: AnyObject])?["message"] as? String {
                                    AlertManager.shared.showSystem(AbstractMessage(type: .info, messagText: serverMsg), actions: [AlertAction(title: "OK", callback: {
                                        resultCallback(apiResponse, statusCode)
                                    })] )
                                } else {
                                    resultCallback(apiResponse, statusCode)
                                }
                            }
        }
    }
    
    func setBaseURL(_ url: String) {
        self.baseURL = url
    }
}
