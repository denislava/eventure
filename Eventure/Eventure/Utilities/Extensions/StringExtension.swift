//
//  StringExtension.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 7/4/16.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import UIKit

extension String {
    
    // MARK: E-mail validation method
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func toImageUrl() -> URL? {
        guard let urlString = self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed),
            let url = URL(string: urlString) else { return nil }
        return url
    }

}

extension String {
    
    var containsOnlyDigits: Bool {
        return Set(self.characters).isSubset(of: digitSet)
    }
    
}

extension String {
    var digitSet: Set<Character> {
        return ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    }
}

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(_ range: Range<Int>) -> String {
        let startIndex = index(from: range.lowerBound)
        let endIndex = index(from: range.upperBound)
        return substring(with: startIndex..<endIndex)
    }
}

extension String {
    func attributedText(fontSize: CGFloat, fontName: String, lineHeight: CGFloat, charactersSpacing: CGFloat = 0) -> NSAttributedString {
        
        let attributedString = NSMutableAttributedString(string: self)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.minimumLineHeight = lineHeight
        paragraphStyle.maximumLineHeight = lineHeight
        
        var attributes: [String: AnyObject] = [:]
        if let font = UIFont(name: fontName, size: fontSize) {
            attributes[NSFontAttributeName] = font
        }
        attributes[NSParagraphStyleAttributeName] = paragraphStyle
        if charactersSpacing > 0 {
            attributes[NSKernAttributeName] = charactersSpacing as AnyObject
        }
        let range = NSRange.init(location: 0, length: attributedString.length)
        attributedString.addAttributes(attributes, range: range)
        
        return attributedString
    }
}
