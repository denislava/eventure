//
//  Action.swift
//  CredoMobileiOS
//
//  Created by Denislava Shentova on 6/19/17.
//  Copyright © 2017 Dyanko Yovchev. All rights reserved.
//

import Foundation

/// Target-Action helper.
final class Action: NSObject {
    
    private let _action: () -> Void
    
    init(action: @escaping () -> Void) {
        _action = action
        super.init()
    }
    
    func action() {
        _action()
    }
    
}
