//
//  JsonProcessor.swift
//  PayTalkAgent
//
//  Created by IO_MAK01 on 3/30/17.
//  Copyright © 2017 Upnetix. All rights reserved.
//

import Foundation

class JsonProcessor {
    typealias ResponseType = [String: AnyObject]
    
    func process(data: Data?) -> DataParseResult<ResponseType> {
        var params: ResponseType?
        guard let data = data else {
            return .failed((description: "Data is nil", system: "System returned: nil", data: "Data contains: nil"))
        }
        
        do {
            params = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String: AnyObject]
            if params == nil,
                let arrParams = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [[String: AnyObject]] {
                params = [:]
                params?["rootArray"] = arrParams as AnyObject?
            }
        } catch {
            var escaped = "\n"
            _ = "\(String(describing: NSString(data: data, encoding: String.Encoding.utf8.rawValue)))".characters.map {
                escaped.append($0)
                if $0 == ">" {
                    escaped.append("\n")
                }
            }
            return .failed((description: "JSON parsing failed!",
                            system: "System returned: \(error)",
                data: "Data contains: \(escaped)"))
        }
        
        return .OK(params ?? [:])
    }
    
    /**
     Deserializing passed params and headers, returning an optional instance of specifically passed child type of DeserializableModel
     */
    func cast<T: DeserializableModel>(incommingHeaders: [AnyHashable: Any]?, processedParams: ResponseType, to: T.Type) -> T? {
        
        // Building the current apiResponse from the json
        let paramAndHeadersDict = combine(headers: incommingHeaders ?? [:],
                                          params: processedParams)
        return T.fromJSON(paramAndHeadersDict) as? T
    }
    
    // Combines the headers and the params into single dictionary to be sent to DeserializableModel.fromJSON()
    // Reason: Some API-s return call-specific data in the header fields, which in some cases we must handle manually.
    private func combine(headers: [AnyHashable: Any], params: [String: AnyObject]) -> [String: AnyObject] {
        var outputDictionary: [String: AnyObject] = [:]
        _ = params.map { outputDictionary[$0.key] = $0.value }
        _ = headers.map { outputDictionary[$0.key.description] = $0.value as AnyObject? }
        return outputDictionary
    }
}
