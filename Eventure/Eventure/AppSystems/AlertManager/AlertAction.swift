//
//  AlertAction.swift
//  PayTalkClient
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import UIKit

struct AlertAction {
    let title: String?
    let style: AlertActionStyle
    let callback: () -> Void
    
    init(title: String?, style: AlertActionStyle = AlertActionStyle.defaultStyle, callback: @escaping () -> Void) {
        self.title = title
        self.style = style
        self.callback = callback
    }
}

struct AlertActionStyle {
    static var defaultStyle: AlertActionStyle = AlertActionStyle(backgroundColor: UIColor.gray, textColor: UIColor.black, textFont: UIFont.systemFont(ofSize: 16))
    
    let backgroundColor: UIColor
    let textColor: UIColor
    let textFont: UIFont
}
