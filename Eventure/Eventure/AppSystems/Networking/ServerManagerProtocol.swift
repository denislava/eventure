//
//  ServerManagerProtocol.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
/**
    Protocol, providing method for async processing of requests and fetching of responses
 */
protocol ServerManagerProtocol {
    var networker: Networker! { get }
    func execute<T: DeserializableModel>(_ params: RequestWrapper, dataProcessor: JsonProcessor, resultCallback: @escaping (_ apiResponse: T?, _ httpStatus: Int) -> Void)
}

enum DataParseResult<R> {
    case OK(R)
    case failed((description: String, system: String, data: String))
}
