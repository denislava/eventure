//
//  SuccessResponse.swift
//  PayTalkAgent
//
//  Created by IO_MAK01 on 3/16/17.
//  Copyright © 2017 Upnetix. All rights reserved.
//

import Foundation
struct SuccessResponse: DeserializableModel {
    let success: Bool
    static func fromJSON(_ jsonData: [String : AnyObject]?) -> DeserializableModel? {
        guard let jsonData = jsonData,
            let result = jsonData["result"] as? [String: Any]
            else {
                Log.put(tag: "SuccessResponse", messages: "Parsing success failed")
                return nil
        }
        //TODO: remove url (when login succes is true server return url...)
        let url = result["url"] as? String ?? ""
        let success = result["success"] as? Bool ?? !url.isEmpty
        return SuccessResponse(success: success)
    }
    
}
