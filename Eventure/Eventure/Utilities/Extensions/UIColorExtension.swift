//
//  UIColorExtension.swift
//  PayTalkAgent
//
//  Created by IO_MAK01 on 3/14/17.
//  Copyright © 2017 Upnetix. All rights reserved.
//

import UIKit
extension UIColor {
    
    open class var mainDarkBlue: UIColor {
        return #colorLiteral(red: 0.1333333333, green: 0.6588235294, blue: 0.8039215686, alpha: 1)
    }
    
    open class var textDarkBlue: UIColor {
        return #colorLiteral(red: 0.1176470588, green: 0.5098039216, blue: 0.6509803922, alpha: 1)
    }
    open class var mainRed: UIColor {
        return #colorLiteral(red: 0.9803921569, green: 0.4, blue: 0.4274509804, alpha: 1)
    }

    open class var offlineGray: UIColor {
        return #colorLiteral(red: 0.8039215686, green: 0.8039215686, blue: 0.8039215686, alpha: 1)
    }
    
    open class var onlineGreen: UIColor {
        return #colorLiteral(red: 0.3019607843, green: 0.937254902, blue: 0.662745098, alpha: 1)
    }
    
    open class var backgroundBlue: UIColor {
        return #colorLiteral(red: 0.9331842661, green: 0.9718707204, blue: 0.9908066392, alpha: 1)
    }
}
