//
//  OptionsCell.swift
//  Eventure
//
//  Created by Denislava on 38//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit

class OptionsCell: UITableViewCell {
    
    @IBOutlet var holderView: UIView!
    @IBOutlet var cellImage: UIImageView!
    @IBOutlet var cellLabel: UILabel!
    
    func populate(image: String, label: String) {
        cellImage.image = UIImage(named: image)
        cellLabel.text = label
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        holderView.layer.cornerRadius = 5        
    }
}
