//
//  UIViewExtension.swift
//  PayTalkAgent
//
//  Created by IO_MAK01 on 3/2/17.
//  Copyright © 2017 Upnetix. All rights reserved.
//

import UIKit
extension UIView {
    /**
     Setup the view to match the size of the screen
     */
    func setup(frame: CGRect) {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        guard let mainView = nib.instantiate(withOwner: self, options: nil)[0] as? UIView else { return }
        self.frame = CGRect(x: 0, y: 0, width: mainView.frame.size.width, height: mainView.frame.size.height)
        self.addSubview(mainView)
        self.frame = frame
        mainView.translatesAutoresizingMaskIntoConstraints = false
        self.pinTo(mainView, attributes: .top, .bottom, .left, .right)
    }
    
}

extension UIView {
    
    func startBlink(duration: Double, delay: Double = 0.0) {
        UIView.animate(withDuration: duration,
                       delay:delay,
                       options:[.autoreverse, .repeat],
                       animations: {
                        self.alpha = 1.0
        }, completion: nil)
    }
    
    func stopBlink() {
        alpha = 1
        layer.removeAllAnimations()
    }
}

extension UIView {
    
    func dropShadow(offsetWidth: CGFloat = -1, offsetHeight: CGFloat = 1, radius: CGFloat = 1) {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: offsetWidth, height: offsetHeight)
        self.layer.shadowRadius = radius
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    }
}

extension UIView {
    /**
     Pin the current view's sides to another view.
     @params: toView - the view to be pinned to
     @params: margin - the desired margin. 0 by default
     @params: attributes: the sides to which you want to pin the view
     */
    func pinTo(_ toView: UIView, margin: CGFloat = 0, attributes: NSLayoutAttribute...) {
        for side in attributes {
            let constraint = NSLayoutConstraint(item: self,
                                                attribute: side,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: toView, attribute: side,
                                                multiplier: 1.0,
                                                constant: margin)
            self.addConstraint(constraint)
        }
    }
}
