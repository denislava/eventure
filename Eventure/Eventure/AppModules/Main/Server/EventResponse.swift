//
//  EventResponse.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

struct EventResponse: DeserializableModel {

    let eventId: Int
    let type: Int
    let date: String
    let location: String
    let isAnswered: IsAnswered

    static func fromJSON(_ jsonData: [String : AnyObject]?) -> DeserializableModel? {
        guard let data = jsonData,
        let eventId = data["eventsId"] as? Int,
        let type = data["eventTypesId"] as? Int,
        let date = data["date"] as? String,
        let location = data["location"] as? String,
            let isAnswered = data["isAnswered"] as? Int,
            let answer = IsAnswered(rawValue: isAnswered)  else { return nil }
        return EventResponse(eventId: eventId, type: type, date: date, location: location, isAnswered: answer)
    }
}

extension EventResponse: Equatable {
    static func ==(lhs: EventResponse, rhs: EventResponse) -> Bool {
        return lhs.eventId == rhs.eventId
    }
}
