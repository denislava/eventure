//
//  Networker.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation

/**
 Protocol for recieving logs instead of printing them in the console if such passed in .withLogging().
 If you wish to handle and process the log data by yourself, pass an instance of the delegate.
 If not leave .withLogging() and it will print the request data. Networker has a pairing functionality
 which assigns identical keys to corresponding requests-responses so a good idea for usage of the delegate
 is to map the response, comming in logResponse() with a previously saved request from logRequest() by their UIDs' so they can be printed together.
 */
protocol NetworkerLoggingDelegate: class {
    /**
     Triggered on sending request
     */
    func logRequest(requestUID: String, request: [String: Any])
    
    /**
     Triggered on response received
     */
    func logResponse(responseUID: String, timestamp: TimeInterval, data: Data?, response: URLResponse?, error: Error?)
}

class Networker {
    private var loggingEnabled: Bool
    private var shouldHandleCookies: Bool
    private var cachingPolicy: URLRequest.CachePolicy
    private var timeout: TimeInterval
    
    // Delegate is not weak on purpose: Nobody but the Networker holds a reference to it so it dies out immediately if it is weak.
    // It should be alive as long as the Networker is alive and should die with it so strong reference in this case is ok.
    // swiftlint:disable weak_delegate
    private var logDelegate: NetworkerLoggingDelegate?
    // swiftlint:enable weak_delegate
    
    /**
     Default constructor returning Networker instance with:
     - disabled logging
     - disabled cookies
     - disabled caching
     - default timeout to 30 secs
     Use the builder methods to enable the above options when necessary
     */
    init() {
        loggingEnabled = false
        shouldHandleCookies = false
        cachingPolicy = .reloadIgnoringLocalAndRemoteCacheData
        timeout = 30
    }
    
    // MARK: Builder options
    
    /**
     Enables the logging. Disabled by default. You can pass a NetworkerLoggingDelegate instance to which the Networker would send the logs, instead of simply printing them.
     */
    func withLogging(logDelegate: NetworkerLoggingDelegate? = nil) -> Networker {
        self.logDelegate = logDelegate
        self.loggingEnabled = true
        return self
    }
    
    /**
     Enables the cookies. Disabled by default
     */
    func withCookies() -> Networker {
        self.shouldHandleCookies = true
        return self
    }
    
    /**
     Enables the caching with revalidating the cache data. Disabled by default
     */
    func withCaching() -> Networker {
        self.cachingPolicy = .reloadRevalidatingCacheData
        return self
    }
    
    /**
     Sets a custom timeout interval. 30 seconds by default
     */
    func withTimeout(secs: TimeInterval) -> Networker {
        self.timeout = secs
        return self
    }
    
    /**
     Swift way to send requests with data and completion handlers.
     Parameters:
     - fullPath: full URL-path of the request
     - method: request method
     - parameters: dictionary of key-value compliant parameters of the request. Optional
     - headers: the headers of the request. Optional
     - completionHandler: the hanlder, which will recieve the control flow on response
     */
    func request(fullPath: String,
                 method: RequestMethod,
                 parameters: [String: Any],
                 headers: [String: String],
                 explicitBody: Data? = nil,
                 completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        var path = fullPath
        var bodyString = ""
        var data: Data? = nil
        
        switch method {
        case .get:
            // Appending the parameters to the URL path in case of GET method
            _ = path.append(urlParams: parameters.toGetParamsUrl())
        case .post, .put, .delete:
            if explicitBody != nil {
                break
            }
            // Initializing the body string with json-represented parameters in case of POST method
            bodyString = parameters.toJSONString()
        }
        
        // Building the URL from the incomming String
        guard let url: URL = URL(string: path) else {
            // TODO: Handle the failed URL - Log and/or give the user feedback for the failed request
            return
        }
        
        // Encoding the incomming parameters dictionary to Data to be appended in httpBody in case of POST
        data = explicitBody ?? bodyString.data(using: String.Encoding.utf8)
        
        // Instantiating a URLRequest with the built URL
        var request1 = URLRequest(url: url)
        
        // Setting the HTTPMethod
        request1.httpMethod = method.rawValue
        
        // Setting the timeout interval
        request1.timeoutInterval = timeout
        
        // Appending the POST data to the body of the request
        request1.httpBody = data
        
        // Appending the incomming headers to the request
        for entry in headers {
            request1.addValue(entry.value, forHTTPHeaderField: entry.key)
        }
        
        // Cookies policy setup
        request1.httpShouldHandleCookies = shouldHandleCookies
        
        // Cache policy setup - Ignore all caching at this phase
        request1.cachePolicy = cachingPolicy
        
        // Generating a UID of the request so the corresponding response could be matched to it in the delegate
        let UID = generateID()
        
        // Explicitly dispatching the completion handler to the main thread
        let handler: (Data?, URLResponse?, Error?) -> Void = { [weak self] data, response, error in
            DispatchQueue.main.async {
                self?.logDelegate?.logResponse(responseUID: UID, timestamp: Date().timeIntervalSince1970, data: data, response: response, error: error)
                completionHandler(data, response, error)
            }
        }
        
        // Logging the request URL, Headers, Parameters, Body and Method
        if loggingEnabled {
            triggerLog(request1: request1, UID: UID)
        }
        Log.put(tag: "REQUEST", messages: request1.description)
        // Executing the request
        URLSession.shared.dataTask(with: request1, completionHandler: handler).resume()
    }
    
    private func generateID() -> String {
        let letters: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< 8 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    private func triggerLog(request1: URLRequest, UID: String) {
        var logData: [String: Any] = [:]
        
        guard
            let url = request1.url,
            let method = request1.httpMethod,
            let headers = request1.allHTTPHeaderFields,
            let requestBody = request1.httpBody
            else { return }
        let bodyString: String = String(data: requestBody, encoding: String.Encoding.utf8) ?? "\(requestBody)"
        
        logData["title"] = "===================   NETWORKER   ==================="
        logData["URL"] = "\(url)"
        logData["method"] = "\(method)"
        logData["headers"] = "\(headers)"
        logData["body"] = "\(bodyString)"
        logData["timestamp"] = Date().timeIntervalSince1970
        logData["requestUID"] = "\(UID)"
        
        if let delegate = logDelegate {
            delegate.logRequest(requestUID: UID, request: logData)
        } else {
            print("|===================   NETWORKER   ===================")
            print("|  Sending request with:")
            print("|____________________________________________________")
            print("|  requestUID:   \(UID)")
            print("|  timestamp:    \(logData["timestamp"] ?? "n/a")")
            print("|")
            print("|  URL:          \(url)")
            print("|  method:       \(method)")
            print("|  headers:      \(headers)")
            print("|  body:         \(bodyString)")
            print("|=====================================================")
        }
    }
}

extension String {
    mutating func append(urlParams: String) -> String {
        self = self.appending(urlParams)
        return self
    }
}

extension Dictionary {
    
    func toJSONString() -> String {
        guard self.count > 0 else { return "" }
        let fromParams = self
        var stringParams: String
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: fromParams, options: JSONSerialization.WritingOptions.prettyPrinted)
            stringParams = String(data: jsonData, encoding: String.Encoding.utf8) ?? "" as String
        } catch let error {
            print(error)
            return ""
        }
        return stringParams
    }
    
    func toGetParamsUrl() -> String {
        guard !self.isEmpty else { return "" }
        let parameterArray = self.map { (key, value) -> String in
            let urlEscapeKey = "\(key)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "\(key)"
            let urlEscapeValue = "\(value)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "\(value)"
            return "\(urlEscapeKey)=\(urlEscapeValue)"
        }
        return "?\(parameterArray.joined(separator: "&"))"
    }
}

enum RequestMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
