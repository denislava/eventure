//
//  UITextFieldExtension.swift
//  PayTalkAgent
//
//  Created by IO_MAK01 on 2/28/17.
//  Copyright © 2017 Upnetix. All rights reserved.
//

import UIKit

extension UITextField {
    func hideKeyboardWhenReturn() {
        self.addTarget(self, action:#selector(hideKeyboard), for:.editingDidEndOnExit)
    }
    
    @objc private func hideKeyboard() {
        self.endEditing(true)
    }
}
