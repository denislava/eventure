//
//  TransparentNavigationController.swift
//  CredoMobileiOS
//
//  Created by Denislava Shentova on 6/13/17.
//  Copyright © 2017 Dyanko Yovchev. All rights reserved.
//

import UIKit

class TransparentNavigationController: UINavigationController {
    
    override class var storyboardName: String {
        return "Login"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.view.backgroundColor = .clear
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
    }
    
}

enum NavigationButtonPosition {
    case right
    case left
}

fileprivate struct LocalConstants {
    static let barButtonItemHeight: CGFloat = 50
    static let barButtonTitleAlphaWhenHighlighted: CGFloat = 0.2
}

extension UINavigationController {
    
    func setCustomNavigationTitleFont(_ navigationBarFont: UIFont?) {
        let font = navigationBarFont ?? UIFont.helveticaNeue()
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white,
                                                 NSFontAttributeName: font]
        
    }
    
    func showBackButton() {
    
        let buttonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action:#selector(popViewController(animated:)))
        self.topViewController?.navigationItem.setLeftBarButton(buttonItem, animated: false)
    }
    
    func setBarButton(title: String, position: NavigationButtonPosition, action: Action?) {
        
        let barButton = UIButton(type: .custom)
        let preferedWidth = NSString(string: title).size(attributes: [NSFontAttributeName: UIFont.helveticaNeue() as Any]).width
        barButton.frame = CGRect(x: 0, y: 0, width: preferedWidth, height: LocalConstants.barButtonItemHeight)
        barButton.setTitle(title, for: .normal)
        barButton.titleLabel?.font = UIFont.helveticaNeue()
        barButton.setTitleColor(barButton.titleColor(for: .normal)?.withAlphaComponent(LocalConstants.barButtonTitleAlphaWhenHighlighted), for: .highlighted)
        
        if let action = action {
            barButton.addTarget(action, action: #selector(action.action), for: .touchUpInside)
        }
        
        let buttonItem = UIBarButtonItem(customView: barButton)
        
        if position == .right {
            self.topViewController?.navigationItem.setRightBarButton(buttonItem, animated: false)
        } else {
            self.topViewController?.navigationItem.setLeftBarButton(buttonItem, animated: false)
        }
    }
    
    func setBarButton(image: UIImage, position: NavigationButtonPosition, action: Action?) {
        
        let barButton = UIBarButtonItem(image: image, style: .plain, target: action, action: #selector(action?.action))
        
        if position == .right {
            self.topViewController?.navigationItem.setRightBarButton(barButton, animated: false)
        } else {
            self.topViewController?.navigationItem.setLeftBarButton(barButton, animated: false)
        }
    }
}
