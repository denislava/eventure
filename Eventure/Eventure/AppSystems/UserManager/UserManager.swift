//
//  UserManager.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

class UserManager {
    
    static let shared = UserManager()
    
    private init() {}
    
    var userId: Int?
    
}
