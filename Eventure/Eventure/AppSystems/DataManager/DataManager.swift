//
//  DataManager.swift
//  Eventure
//
//  Created by Denislava on 128//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation
import FacebookCore

class DataManager {
    
    static let local = DataManager()
    private init() {}
    
    var userId: String? {
        return AccessToken.current?.userId
    }
    var defaultAddress: String? {
        return UserDefaults.standard.string(forKey: UserDefaultsConstants.defaultAddressKey)
    }
    
    func saveDefaultAddress(address: String) {
        UserDefaults.standard.set(address, forKey: UserDefaultsConstants.defaultAddressKey)
        UserDefaults.standard.synchronize()
    }
}
