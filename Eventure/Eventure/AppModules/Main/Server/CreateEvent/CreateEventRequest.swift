//
//  CreateEventRequest.swift
//  Eventure
//
//  Created by Denislava on 69//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation

class CreateEventRequest: BaseRequest {
    
    struct Params: SerializableModel {
        let userId: Int
        let type: Int
        let date: String
        let location: String
        let isAnswered: Int
        
        func toJSON() -> [String: Any] {
            var params: [String: Any] = [:]
            params["usersId"] = userId
            params["eventTypesId"] = type
            params["date"] = date
            params["location"] = location
            params["isAnswered"] = isAnswered
            return params
        }
    }
    
    init(userId: Int, type: Int, date: String, location: String, isAnswered: Int) {
        super.init(params: Params(userId: userId, type: type, date: date, location: location, isAnswered: isAnswered))
    }
}
