//
//  UIColorExtention.swift
//  Eventure
//
//  Created by Denislava on 28//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    public class var darkViolet: UIColor {
        return UIColor(red: 122/255, green: 53/255, blue: 108/255, alpha: 1.0)
    }
    
    public class var lightViolet: UIColor {
        return UIColor(red: 177/255, green: 117/255, blue: 177/255, alpha: 1.0)
    }
    
    public class var rose: UIColor {
        return UIColor(red: 234/255, green: 192/255, blue: 228/255, alpha: 1.0)
    }
    
    public class var beige: UIColor {
        return UIColor(red: 241/255, green: 230/255, blue: 224/255, alpha: 1.0)
    }
    
    public class var lightBeige: UIColor {
        return UIColor(red: 239/255, green: 235/255, blue: 232/255, alpha: 1.0)
    }
    
    public class var lightPurple: UIColor {
        return UIColor(red: 147/255, green: 65/255, blue: 166/255, alpha: 1.0)
    }
    
    public class var darkPurple: UIColor {
        return UIColor(red: 92/255, green: 48/255, blue: 121/255, alpha: 1.0)
    }
    
    public class var lightGreen: UIColor {
        return UIColor(red: 170/255, green: 226/255, blue: 203/255, alpha: 1.0)
    }
    
    public class var darkGreen: UIColor {
        return UIColor(red: 107/255, green: 198/255, blue: 189/255, alpha: 1.0)
    }
    
    public class var mySoul: UIColor {
        return UIColor(red: 47/255, green: 11/255, blue: 48/255, alpha: 1.0)
    }
}
