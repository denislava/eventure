//
//  ValidatableUIView.swift
//  PayTalkAgent
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

class ValidatableUIView: UIView, Validatable, HighlightsOnFail {
    var message: () -> String = {
        print("Error in ValidableTextField: invoiking isValid() while validation rule not set!")
        return ""
    }
    
    var isValid: () -> Bool = { return false }
    
    func setValidationRule(message: String, rule: @escaping (String) -> Bool) {
        self.isValid = {
            return rule("")
        }
        
        self.message = {
            return message
        }
    }
    
    func getBorderColor(for state: Bool) -> CGColor {
        return state ? ValidatableUISettings.TextViewProperties.borderColor : ValidatableUISettings.TextViewProperties.failedBorderColor
    }
    
    func getBackgroundColor(for state: Bool) -> UIColor {
        return state ? ValidatableUISettings.TextViewProperties.bgColor : ValidatableUISettings.TextViewProperties.failedBackground
    }
    
    func getBorderWidth(for state: Bool) -> CGFloat {
        return state ? ValidatableUISettings.TextViewProperties.borderWidth : ValidatableUISettings.TextViewProperties.failedBorderWidth
    }
}
