//
//  AppDelegate.swift
//  Eventure
//
//  Created by Denislava on 28//17.
//  Copyright © 2017 DSH. All rights reserved.
//

import UIKit
import FacebookCore
import HockeySDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    private let hockeyId = "d7073ea46a514500950101dc4a8d31d1"
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        ///Hockey
        BITHockeyManager.shared().configure(withIdentifier: hockeyId)
        BITHockeyManager.shared().start()
        BITHockeyManager.shared().authenticator.authenticateInstallation()
        
        /// FB SDK Delegate
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        ///Server Manager
        AlertManager.instantiate()
        ServerManager.instantiate(networker: Networker(), alertManager: AlertManager.shared, baseURL: Server.baseUrl)
        /// Nav Bar
        UIToolbar.appearance().tintColor = UIColor.darkViolet
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return SDKApplicationDelegate.shared.application(app, open: url, options: options)
    }

}
