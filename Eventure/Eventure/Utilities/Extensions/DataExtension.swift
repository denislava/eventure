//
//  DataExtension.swift
//  PayTalkAgent
//
//  Created by Rumyana Atanasova on 26.03.17 г..
//  Copyright © 2017 г. Upnetix. All rights reserved.
//

import Foundation

extension Data {
    
    /// Append string to NSMutableData
    ///
    /// Rather than littering my code with calls to `dataUsingEncoding` to convert strings to NSData,
    /// and then add that data to the NSMutableData, this wraps it in a nice convenient little extension to NSMutableData.
    /// This converts using UTF-8.
    ///
    /// - parameter string:       The string to be added to the `NSMutableData`.
    
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}

extension Data {
    /// Method for obtaining the content-type for a multipart request form
    ///
    /// - Returns: the Content-Type of a data if it's an image, else nil
    func contentTypeForImageData() -> String? {
        switch self.first {
        case 0xFF?:
            return "image/jpeg"
        case 0x89?:
            return "image/png"
        case 0x47?:
            return "image/gif"
        case 0x49?:
            fallthrough
        case 0x4D?:
            return "image/tiff"
        default:
            return nil
        }
    }
}
